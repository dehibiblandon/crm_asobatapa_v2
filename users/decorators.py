# decorators.py

from django.conf import settings
from functools import wraps
from django.http import HttpResponseRedirect
from django.urls import reverse

def restrict_to_groups(*allowed_roles):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            user = request.user
            if user.groups.filter(name__in=allowed_roles).exists():
                return view_func(request, *args, **kwargs)
            else:
                return HttpResponseRedirect(reverse('banners:login'))
        return _wrapped_view
    return decorator

