from django.contrib import admin
from .resources import SuscriberResource
from .models import Sector, Actions, Suscriber, CostActions
from import_export import resources

from import_export.admin import ImportExportModelAdmin

# Register your models here
class CustomSuscriberAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = SuscriberResource
    list_display = ("name_suscriber", "last_name_suscribe", "sector_suscriber", "company", "created_date",)
    list_filter = ["sector_suscriber"]
    search_fields = ["name_suscriber"]


admin.site.register(Sector)
admin.site.register(CostActions)
admin.site.register(Actions)
admin.site.register(Suscriber, CustomSuscriberAdmin)