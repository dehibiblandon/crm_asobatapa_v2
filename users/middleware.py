from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect

class UsuarioAutenticadoMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.user_is_authenticated = request.user
        response = self.get_response(request)
        return response

class EmpresaMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if request.user.is_authenticated:
            request.empresa = request.user.profile.empresa
        else:
            request.empresa = None