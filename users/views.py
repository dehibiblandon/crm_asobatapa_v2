from django.shortcuts import render
from django.db.models import Sum
# Create your views here.
from django.http import JsonResponse
from django.shortcuts import render, redirect
from .models import Suscriber, CostActions
from inventory.models import OrdenServicio
from payments.models import CashBalance, Invoice
from common.models import UserProfile, Projects
from .form import SuscriberForm
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views import View
from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.conf import settings
from django.http import HttpResponseRedirect
from django.contrib.messages.views import SuccessMessageMixin
import calendar
from django.utils import timezone
from datetime import datetime
import locale

def custom_redirect_to_login(request):
    return HttpResponseRedirect('usuario/login/')

class CustomLoginView(LoginView):
    template_name = 'registration/sign-in.html'

    def get_success_url(self):
        return reverse_lazy(self.request.GET.get('next', None) or settings.LOGIN_REDIRECT_URL)

    def get_login_url(self):
        return reverse_lazy(settings.LOGIN_URL)

    def form_valid(self, form):
        response = super().form_valid(form)
        user = self.request.user

        # Si es admin
        if user.is_authenticated and user.is_staff:
            return redirect('admin:index')

        # Verificamos grupo de usuario y redirigimos adecuadamente
        if user.is_authenticated:
            if user.groups.filter(name='tesorera').exists():
                return redirect('transversal:dashboard')

        return response

class CustomLogoutView(LogoutView):
    next_page = 'transversal:login'

@method_decorator(login_required(login_url='transversal:login'), name='dispatch')
class CustomDashboardView(TemplateView):
    template_name = 'dashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        # Obtener la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company

        # Configurar el locale
        original_locale = locale.getlocale()
        try:
            locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
        except locale.Error:
            locale.setlocale(locale.LC_TIME, 'C')
        
        # Obtener el mes actual en español
        now = timezone.now()
        mes_actual = now.strftime('%B')
        
        # Calcular el primer y último día del mes
        first_day_of_month = now.replace(day=1)
        last_day_of_month = (first_day_of_month + timezone.timedelta(days=32)).replace(day=1) - timezone.timedelta(days=1)

        # Consulta cantidad de ingresos en el periodo actual
        ingresos_mes_actual = CashBalance.objects.filter(
            company=user_company,
            transaction_type='Ingreso',
            created_date__gte=first_day_of_month,
            created_date__lte=last_day_of_month
        ).aggregate(total_ingresos=Sum('amount'))

        total_ingresos = ingresos_mes_actual['total_ingresos'] or 0

        # Consultar y contar la cantidad de personas que han pagado
        suscriptores_pagados = Invoice.objects.filter(
            company=user_company,
            payment_status='paid',
            last_payment_date__gte=first_day_of_month,
            last_payment_date__lte=last_day_of_month
        ).values('suscriber').distinct().count()

        # Consultar la cantidad de egresos en el periodo actual
        egresos_mes_actual = CashBalance.objects.filter(
            company=user_company,
            transaction_type='Egreso',
            created_date__gte=first_day_of_month,
            created_date__lte=last_day_of_month
        ).aggregate(total_egresos=Sum('amount'))

        total_egresos = egresos_mes_actual['total_egresos'] or 0

        # Consultar la cantidad de saldo en caja
        current_balance = CashBalance.get_current_balance(self.request)

        # Consultar todos los suscriptores y sumar los valores de las acciones afiliadas
        total_monthly_payment = Suscriber.objects.filter(company=user_company).aggregate(total_monthly_payment=Sum('monthly_value_to_pay'))['total_monthly_payment'] or 0

        # Suscriptores con facturas vencidas
        suscribers_con_facturas_vencidas = Suscriber.objects.filter(company=user_company, invoices__payment_status='expired').distinct().count()

        # Proyectos
        projects = Projects.objects.filter(company=user_company)

        # Restaurar el locale original
        locale.setlocale(locale.LC_TIME, original_locale)

        # Actualizar el contexto
        context['total_monthly_payment'] = total_monthly_payment
        context['usuario'] = self.request.user
        context['suscribers_con_facturas_vencidas'] = suscribers_con_facturas_vencidas
        context['current_balance'] = current_balance
        context['total_ingresos'] = total_ingresos
        context['mes_actual'] = mes_actual
        context['suscriptores_pagados'] = suscriptores_pagados
        context['total_egresos'] = total_egresos
        context['projects'] = projects

        return context    
class MonthlyIncomeView(View):
    def get(self, request, *args, **kwargs):
        mes = request.GET.get('mes')  # Mes en formato numérico (e.g., '3' para marzo)
        anio = request.GET.get('anio')  # Año en formato numérico (e.g., '2024')
        
        # Si el año no está especificado, toma el año actual
        if not anio:
            anio = timezone.now().year

        # Filtrar los ingresos del mes seleccionado
        user_company = UserProfile.objects.get(user=request.user).company  # Obtén la compañía del usuario
        # Definir las fechas de inicio y fin del mes seleccionado
        
        ingresos = CashBalance.objects.filter(
            company=user_company,
            transaction_type='Ingreso',
            created_date__year=anio,
            created_date__month=mes
        ).aggregate(total_ingreso=Sum('amount'))['total_ingreso'] or 0
                
        egresos = CashBalance.objects.filter(
            company=user_company,
            transaction_type='Egreso',
            created_date__year=anio,
            created_date__month=mes
        ).aggregate(total_egreso=Sum('amount'))['total_egreso'] or 0

   

        return JsonResponse({'total_ingreso': ingresos, 'total_egreso': egresos})

class SuscriberListView(ListView):
    model = Suscriber
    template_name = 'suscriber/suscriber_list.html'
    context_object_name = 'suscribers'

class SuscriberDetailView(DetailView):
    model = Suscriber
    template_name = 'suscriber_detail.html'
    context_object_name = 'suscriber'

class SuscriberCreateView(CreateView):
    model = Suscriber
    template_name = 'suscriber/suscriber_form.html'    
    context_object_name = "suscribe"
    form_class = SuscriberForm
    success_url = reverse_lazy('transversal:suscriber_list')
    success_message = "Suscriptor Creado"

    # agregamos el usuario que realiza la modificación
    def form_valid(self, form):
        
        form.instance.user_modified = self.request.user.username
        form.instance.created_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        

        response = super().form_valid(form)
        # Calcula la suma del valor de las acciones y las resta con el valor pagado al inscribirse
        total_value_action_suscribe = sum(action_cost.costo_uniqued_for_action for action_cost in form.cleaned_data['cost_action'])
        form.instance.pending_balance = total_value_action_suscribe - form.cleaned_data['value_paid_for_action']
        # Calcular la suma de los valores de value_action de las acciones afiliadas
        total_value_action = sum(action.value_action for action in form.cleaned_data['affiliated_shares'])
        form.instance.monthly_value_to_pay = total_value_action



        # Guardar el objeto Suscriber nuevamente después de realizar operaciones adicionales
        form.instance.save()

        return response

class SuscriberUpdateView(UpdateView):
    model = Suscriber
    template_name = 'suscriber/suscriber_form.html'
    success_url = reverse_lazy('transversal:suscriber_list')
    context_object_name = "suscriber"
    form_class = SuscriberForm
    success_message = "Suscriptor Actulizado"

    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.modified_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)

        # Calcula la suma del valor de las acciones y las resta con el valor pagado al inscribirse
        total_value_action_suscribe = sum(action_cost.costo_uniqued_for_action for action_cost in form.cleaned_data['cost_action'])
        form.instance.pending_balance = total_value_action_suscribe - form.cleaned_data['value_paid_for_action']

        # Calcular la suma de los valores de value_action de las acciones afiliadas
        total_value_action = sum(action.value_action for action in form.cleaned_data['affiliated_shares'])
        form.instance.monthly_value_to_pay = total_value_action

        # Guardar el objeto Suscriber nuevamente después de realizar operaciones adicionales
        form.instance.save()
        return response

class SuscriberDeleteView(DeleteView):
    model = Suscriber
    success_url = reverse_lazy('transversal:suscriber_list')

    def delete(self, request, *args, **kwargs):
        suscriber = self.get_object()
        suscriber.delete()
        return JsonResponse({'success': True, 'message': 'Suscriptor eliminado exitosamente.'})
    
class SuscriberListDelinquent(TemplateView):
    template_name = 'suscriber/suscriber_list_delinquent.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        suscribers_con_facturas_vencidas = Suscriber.objects.filter(invoices__payment_status='expired').distinct()
        context['suscribers'] = suscribers_con_facturas_vencidas
        return context
    

def suspended_suscriber(request, suscriber_id):
    if request.method == 'POST':
        try:
            # Obtener el suscriptor por su ID
            suscriber = Suscriber.objects.get(pk=suscriber_id)
            suscriber.status = 'suspended'
            suscriber.save()
            #Generar registro de Orden de trabajo para el modelo OrdenServicio
            
            orden_servicio = OrdenServicio.objects.create(
                name = f"Suspensión de servicio para {suscriber.name_suscriber} {suscriber.last_name_suscribe}",
                suscriber_asociate = suscriber,
                type_service = 'suspend/reactivate',
                comentary_service="Suspensión automática debido a mora."
            )
            
            return JsonResponse({'success': True, 'message': 'Suscriptor Suspendido.'})
        except Suscriber.DoesNotExist:
            # Si la factura no existe, devolver un mensaje de error
            return JsonResponse({'success': False, 'message': 'El Suscriptor no existe'}) 
        
def enable_suscriber(request, suscriber_id):
    if request.method == 'POST':
        try:
            # Obtener el suscriptor por su ID
            suscriber = Suscriber.objects.get(pk=suscriber_id)
            suscriber.status = 'active'
            suscriber.save()
            #Generar registro de Orden de trabajo para el modelo OrdenServicio
            
            orden_servicio = OrdenServicio.objects.create(
                name = f"Habilitación de servicio para {suscriber.name_suscriber} {suscriber.last_name_suscribe}",
                suscriber_asociate = suscriber,
                type_service = 'suspend/reactivate',
                comentary_service="Habilitación para suscriptor.",
            )
            
            return JsonResponse({'success': True, 'message': 'Suscriptor Habilitado.'})
        except Suscriber.DoesNotExist:
            # Si la factura no existe, devolver un mensaje de error
            return JsonResponse({'success': False, 'message': 'El Suscriptor no existe'})
        
