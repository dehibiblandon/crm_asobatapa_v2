from import_export import fields, resources
from .models import Actions, Sector, Suscriber, CostActions
from common.models  import Company
from import_export.widgets import ForeignKeyWidget, ManyToManyWidget

class SuscriberResource(resources.ModelResource):
    company = fields.Field(
        column_name='Company',
        attribute='company',
        widget=ForeignKeyWidget(Company, field='name_company')
    )
    sector_suscriber = fields.Field(
        column_name='Sector',
        attribute='sector_suscriber',
        widget=ForeignKeyWidget(Sector, field='name_sector')
    )
    
    cost_action = fields.Field(
        column_name='Valor Acción',
        attribute='cost_action',
        widget=ManyToManyWidget(CostActions, field='costo_uniqued_for_action')
    )

    affiliated_shares = fields.Field(
        column_name='Cantidad Acciones Asociadas',
        attribute='affiliated_shares',
        widget=ManyToManyWidget(Actions, field='value_action')
    )

    class Meta:
        model = Suscriber
        fields = ('id', 'sector_suscriber', 'company', 'document_type', 'identification_number', 'name_suscriber', 'last_name_suscribe', 'telephone', 'name_farm', 'stratum', 'count_adult_inhabitants', 'count_children_inhabitants',
                  'value_paid_for_action', 'number_cuotes_for_payment', 'monthly_value_to_pay', 'pending_balance', 'last_payment_date', 'status')
        import_id_fields = ('id',)

