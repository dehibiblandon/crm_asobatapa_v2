# Generated by Django 5.0.7 on 2024-07-20 00:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='actions',
            options={'verbose_name': 'Valor de Uso Por tipo de acción', 'verbose_name_plural': 'Valores de Uso Por tipo de acción'},
        ),
        migrations.AlterModelOptions(
            name='costactions',
            options={'verbose_name': 'Valor de la Acción (MATRÍCULA) Definida'},
        ),
    ]
