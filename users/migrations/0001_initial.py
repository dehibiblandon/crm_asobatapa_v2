# Generated by Django 5.0.3 on 2024-05-31 01:38

import django.db.models.deletion
import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Actions',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Publicación')),
                ('modified_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Actualización')),
                ('user_modified', models.CharField(blank=True, max_length=150, null=True, verbose_name='Usuario que Crea o Modifica')),
                ('value_action', models.IntegerField(default=0, verbose_name='Tarifa Mensual Por Acción')),
                ('property_type', models.CharField(choices=[('not use', 'No Usa Acción'), ('house Alone', 'Vivienda Desocupada'), ('house', 'Vivienda'), ('Cattle Farm', 'Finca Ganadera'), ('Country Farm', 'Finca Campestre'), ('Farming Farm', 'Finca Agropecuaria'), ('Pork Farm', 'Finca Porcicola'), ('Other', 'Otro Negocio')], default='house', max_length=15)),
                ('company', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='%(class)s_empresa', to='common.company')),
            ],
            options={
                'verbose_name': 'Valor de Acción',
                'verbose_name_plural': 'Valores de Acciones',
            },
        ),
        migrations.CreateModel(
            name='CostActions',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Publicación')),
                ('modified_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Actualización')),
                ('user_modified', models.CharField(blank=True, max_length=150, null=True, verbose_name='Usuario que Crea o Modifica')),
                ('costo_uniqued_for_action', models.IntegerField(default=0, verbose_name='Valor de la Acción')),
                ('company', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='%(class)s_empresa', to='common.company')),
            ],
            options={
                'verbose_name': 'Valor de la Acción Definida',
            },
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Publicación')),
                ('modified_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Actualización')),
                ('user_modified', models.CharField(blank=True, max_length=150, null=True, verbose_name='Usuario que Crea o Modifica')),
                ('name_sector', models.CharField(default='', max_length=55, verbose_name='Nombre Sector')),
                ('company', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='%(class)s_empresa', to='common.company')),
            ],
            options={
                'verbose_name': 'Sector o Vereda',
                'verbose_name_plural': 'Sectores o Veredas',
            },
        ),
        migrations.CreateModel(
            name='Suscriber',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Publicación')),
                ('modified_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Actualización')),
                ('user_modified', models.CharField(blank=True, max_length=150, null=True, verbose_name='Usuario que Crea o Modifica')),
                ('document_type', models.CharField(choices=[('no', 'Seleccionar'), ('CC', 'C.C'), ('CE', 'C.E')], default='CC', max_length=2, verbose_name='Tipo de Documento')),
                ('identification_number', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Documento')),
                ('name_suscriber', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Nombre de suscriptor')),
                ('last_name_suscribe', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Apellido del Suscriptor')),
                ('telephone', models.CharField(max_length=15)),
                ('name_farm', models.CharField(default='', max_length=55, verbose_name='Nombre Finca')),
                ('stratum', models.CharField(blank=True, default='', max_length=3, null=True, verbose_name='Estrato Vivienda')),
                ('count_adult_inhabitants', models.IntegerField(blank=True, default=0, null=True, verbose_name='Cantidad Adultos')),
                ('count_children_inhabitants', models.IntegerField(blank=True, default=0, null=True, verbose_name='Cantidad Niños')),
                ('value_paid_for_action', models.IntegerField(default=0, verbose_name='Valor qué pagó de la acción al Suscribirse')),
                ('number_cuotes_for_payment', models.IntegerField(default=0, verbose_name='Cantidad de cuotas a diferir el Pago')),
                ('monthly_value_to_pay', models.IntegerField(default=0, verbose_name='Tarifa Mensual Servicio (Generado Automáticamente Segun el Tipo de Acción)')),
                ('pending_balance', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=10, null=True, verbose_name='Saldo Pendiente por el Costo de Acción (Generado Automáticamente)')),
                ('last_payment_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Fecha de último Pago')),
                ('status', models.CharField(choices=[('active', 'Activo Vigente'), ('suspended', 'Suspendido Mora')], default='active', max_length=20, verbose_name='Estado de Suscriptor')),
                ('mode_billing', models.CharField(choices=[('frequently', 'Frecuente'), ('annual', 'Anual')], default='frequently', max_length=20, verbose_name='Modo de facturación para el suscriptor')),
                ('affiliated_shares', models.ManyToManyField(blank=True, null=True, to='users.actions', verbose_name='Cantidad de Suscripciones Asociadas')),
                ('company', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='%(class)s_empresa', to='common.company')),
                ('cost_action', models.ManyToManyField(blank=True, null=True, to='users.costactions', verbose_name='Costo de la Acción Suscrita')),
                ('sector_suscriber', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.sector', verbose_name='Sector')),
            ],
            options={
                'verbose_name': 'Suscriptor',
                'verbose_name_plural': 'Suscriptores',
            },
        ),
    ]
