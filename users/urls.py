from django.contrib import admin
from django.urls import path
from users.views import (
    CustomLoginView, 
    CustomLogoutView, 
    CustomDashboardView, 
    SuscriberListView, 
    SuscriberDetailView, 
    SuscriberCreateView, 
    SuscriberUpdateView, 
    SuscriberDeleteView,
    SuscriberListDelinquent,
    MonthlyIncomeView,
    suspended_suscriber,
    enable_suscriber,
    )
app_name = 'transversal'
urlpatterns = [
    path('login/', CustomLoginView.as_view(), name='login'),
    path('logout/', CustomLogoutView.as_view(), name='logout'),
    path('dashboard', CustomDashboardView.as_view(), name='dashboard'),
    path('monthly-income/', MonthlyIncomeView.as_view(), name='monthly_income'),
    path('suscriptores/', SuscriberListView.as_view(), name='suscriber_list'),
    path('suscriptores/<int:pk>/', SuscriberDetailView.as_view(), name='suscriber_detail'),
    path('suscriptores/new/', SuscriberCreateView.as_view(), name='suscriber_create'),
    path('suscriptores/<int:pk>/edit/', SuscriberUpdateView.as_view(), name='suscriber_edit'),
    path('suscriptores/<int:pk>/eliminar/', SuscriberDeleteView.as_view(), name='suscriber_delete'),
    path('suscriptores/en-mora', SuscriberListDelinquent.as_view(), name='suscribers_delinquent'),
    path('suscriptores/suspender-servicio/<int:suscriber_id>', suspended_suscriber, name='suspender_servicio'),
    path('suscriptores/habilitar-servicio/<int:suscriber_id>', enable_suscriber, name='habilitar_servicio'),

]