from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from common.models import BaseManage

   
class Sector(BaseManage):
    name_sector = models.CharField(
        verbose_name="Nombre Sector",
        max_length=55,
        blank=False,
        null=False,
        default=''
    )
    def __str__(self):
        return self.name_sector

    class Meta:
        verbose_name = "Sector o Vereda"
        verbose_name_plural = "Sectores o Veredas"
        
class CostActions(BaseManage):
    costo_uniqued_for_action = models.IntegerField(
        verbose_name="Valor de la Acción",
        blank=False,
        null=False,
        default=0
    )
    def __str__(self):
        return str(self.costo_uniqued_for_action)

    class Meta:
        verbose_name = "Valor de la Acción (MATRÍCULA) Definida"

class Actions(BaseManage):
    value_action = models.IntegerField(
        verbose_name="Tarifa Mensual Por Acción",
        blank=False,
        null=False,
        default=0
    )
    property_type = models.CharField(
        max_length=15,
        choices=[
            ('not use', 'No Usa Acción'),
            ('house Alone', 'Vivienda Desocupada'),
            ('house', 'Vivienda'),
            ('Cattle Farm', 'Finca Ganadera'),
            ('Country Farm', 'Finca Campestre'),
            ('Farming Farm', 'Finca Agropecuaria'),
            ('Pork Farm', 'Finca Porcicola'),
            ('Other', 'Otro Negocio'),
        ],
        default='house',
    )

    def get_property_type_display(self):
            for choice in self._meta.get_field('property_type').choices:
                if choice[0] == self.property_type:
                    return choice[1]
            return self.property_type

    def __str__(self):
            return f"{self.get_property_type_display()} - {self.value_action}"

    class Meta:
        verbose_name = "Valor de Uso Por tipo de acción"
        verbose_name_plural = "Valores de Uso Por tipo de acción"

class Suscriber(BaseManage):
    PROPERTY_TYPE_CHOICES = (
        ('not use', 'No Usa'),
        ('house Alone', 'Vivienda Sola'),
        ('house', 'Vivienda'),
        ('Cattle Farm', 'Finca Ganadera'),
        ('Country Farm', 'Finca Campestre'),
        ('Farming Farm', 'Finca Agropecuaria'),
        ('Pork Farm', 'Finca Porcicola'),
        ('Other', 'Otro Negocio'),
    )
    DOCUMENT_TYPE_CHOICES = [
        ('no', 'Seleccionar'),
        ('CC', 'C.C'),
        ('CE', 'C.E'),
    ]
    document_type = models.CharField(
        verbose_name="Tipo de Documento",
        max_length=2,
        choices=DOCUMENT_TYPE_CHOICES,
        default='CC',
    )
    identification_number = models.CharField(
        verbose_name="Documento",
        max_length=255,
        blank=True,
        null=True,
        default=''
    )
    name_suscriber = models.CharField(
        verbose_name="Nombre de suscriptor",
        max_length=255,
        blank=True,
        null=True,
        default=''
    )
    last_name_suscribe = models.CharField(
        verbose_name="Apellido del Suscriptor",
        max_length=255,
        blank=True,
        null=True,
        default=''
    )
    telephone = models.CharField(max_length=15)
    sector_suscriber = models.ForeignKey(
        Sector,
        verbose_name="Sector",
        on_delete=models.CASCADE,
    )
    name_farm = models.CharField(
        verbose_name="Nombre Finca",
        max_length=55,
        blank=False,
        null=False,
        default=''
    )
    stratum = models.CharField(
        verbose_name="Estrato Vivienda",
        max_length=3,
        blank=True,
        null=True,
        default=''
    )
    count_adult_inhabitants = models.IntegerField(
        verbose_name="Cantidad Adultos",
        blank=True,
        null=True,
        default=0
    )
    count_children_inhabitants = models.IntegerField(
        verbose_name="Cantidad Niños",
        blank=True,
        null=True,
        default=0
    )
    cost_action = models.ManyToManyField(
        CostActions,
        blank=True,
        null=True,
        verbose_name= "Costo de la Acción Suscrita"
    )
    affiliated_shares = models.ManyToManyField(
        Actions,
        verbose_name="Cantidad de Suscripciones Asociadas",
        blank=True,
        null=True,
    )
    value_paid_for_action = models.IntegerField(
        verbose_name="Valor qué pagó de la acción al Suscribirse",
        blank=False,
        null=False,
        default=0
    )
    number_cuotes_for_payment = models.IntegerField(
        verbose_name="Cantidad de cuotas a diferir el Pago",
        blank=False,
        null=False,
        default=0
    )
    monthly_value_to_pay = models.IntegerField(
        verbose_name="Tarifa Mensual Servicio (Generado Automáticamente Segun el Tipo de Acción)",
        blank=False,
        null=False,
        default=0
    )
    pending_balance = models.DecimalField(
        verbose_name="Saldo Pendiente por el Costo de Acción (Generado Automáticamente)",
        max_digits=10,
        decimal_places=2,
        default=0,
        blank=True,
        null=True
    )
    last_payment_date = models.DateTimeField(
        verbose_name="Fecha de último Pago",
        default='',
        blank=True,
        null=True
    )
    status= models.CharField(
        max_length=20,
        choices=[('active', 'Activo Vigente'), ('suspended', 'Suspendido Mora')],
        default='active',
        verbose_name="Estado de Suscriptor"
    )
    mode_billing = models.CharField(
        max_length=20,
        choices=[('frequently', 'Frecuente'), ('annual', 'Anual')],
        default='frequently',
        verbose_name="Modo de facturación para el suscriptor"
    )
    def __str__(self):
        return self.name_suscriber

    class Meta:
        verbose_name = "Suscriptor"
        verbose_name_plural = "Suscriptores"

    # def update_pending_balance(self):
    #     total_pending = self.payment_set.aggregate(total=models.Sum('total_amount'))['total'] or 0
    #     self.pending_balance = total_pending
    #     self.save()