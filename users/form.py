from django import forms
from .models import Suscriber,Actions, CostActions

# class PurchaseForm(forms.ModelForm):
#     class Meta:
#         model = Suscriber
#         fields = ['name', 'observation', 'num_invoice', 'date_invoice', 'sub_total', 'discount']
#         widget = {'observation': forms.TextInput}

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         for field in iter(self.fields):
#             self.fields[field].widget.attrs.update({'class': 'form-control'})


class SuscriberForm(forms.ModelForm):
    class Meta:
        model = Suscriber
        fields = [
            'document_type',
            'identification_number',
            'name_suscriber',
            'last_name_suscribe',
            'telephone',
            'sector_suscriber',
            'name_farm',
            'stratum',
            'count_adult_inhabitants',
            'count_children_inhabitants',
            'cost_action',
            'affiliated_shares',
            'value_paid_for_action',
            'number_cuotes_for_payment',
            'monthly_value_to_pay',
            'pending_balance',
            'last_payment_date',
            'mode_billing',
            'status'
        ]

        widgets = {
            'document_type': forms.Select(attrs={'class': 'form-control'}),
            'sector_suscriber': forms.Select(attrs={'class': 'form-control'}),
            'cost_action': forms.SelectMultiple(attrs={'class': 'form-control'}),
            'affiliated_shares': forms.SelectMultiple(attrs={'class': 'form-control'}),
            'mode_billing': forms.Select(attrs={'class': 'form-control'}),
            'status': forms.Select(attrs={'class': 'form-control'}),
           }

    cost_action = forms.ModelMultipleChoiceField(
        queryset=CostActions.objects.all(),
        widget=forms.SelectMultiple(attrs={'class': 'form-control'}),
        required=True  # Puedes ajustar esto según tus necesidades
    )    
    affiliated_shares = forms.ModelMultipleChoiceField(
        queryset=Actions.objects.all(),
        widget=forms.SelectMultiple(attrs={'class': 'form-control'}),
        required=True  # Puedes ajustar esto según tus necesidades
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

   