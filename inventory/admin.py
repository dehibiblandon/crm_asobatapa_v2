from django.contrib import admin
from .models import Provider, Category, Inventory, Shopping, ShoppingDetail, Product, OrdenServicio

# Register your models he

admin.site.register(Provider)
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Shopping)
admin.site.register(ShoppingDetail)
admin.site.register(Inventory)
admin.site.register(OrdenServicio)