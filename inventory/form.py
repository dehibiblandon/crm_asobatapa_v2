from django import forms
from .models import  Provider, Shopping, ShoppingDetail, Category, Product, Inventory, OrdenServicio

# class PurchaseForm(forms.ModelForm):
#     class Meta:
#         model = Suscriber
#         fields = ['name', 'observation', 'num_invoice', 'date_invoice', 'sub_total', 'discount']
#         widget = {'observation': forms.TextInput}

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         for field in iter(self.fields):
#             self.fields[field].widget.attrs.update({'class': 'form-control'})

class ProviderForm(forms.ModelForm):
    class Meta:
        model = Provider
        fields = [
            'name',
            'description',
            'address',
            'contact',
            'telephone',
            'email'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class ShoppingForm(forms.ModelForm):
    class Meta:
        model = Shopping
        fields = [
            'supplier_asociate',
            'purchase_date',
            'number_invoice_provider',            
        ]
        widgets = {
            'purchase_date': forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class ShoppingDetailForm(forms.ModelForm):
    class Meta:
        model = ShoppingDetail
        fields = [
            'purchase',
            'product',
            'amount',
            'price',
            'sub_total',
            'discount',
            'total',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            'name',
            'description',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            'category',
            'name',
            'description',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class InventoryForm(forms.ModelForm):
    class Meta:
        model = Inventory
        fields = [
            'name',
            'product',
            'current_existence',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})


class OrdenServicioForm(forms.ModelForm):
    class Meta:
        model = OrdenServicio
        fields = [
            'name',
            'suscriber_asociate',
            'comentary_service',
            'product_used',
            'close_date',
            'status_orders_service',
            'type_service',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})