from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models.signals import pre_save
from django.dispatch import receiver
from users.models import BaseManage, Suscriber
from payments.models import CashBalance
from django.db.models import Sum
from common.models import BaseManage
# Create your models here

class Provider(BaseManage):
    name = models.CharField(
        max_length=150,
        verbose_name="Nombre del proveedor",
        null=False,
        blank= False,
    )
    description = models.CharField(
        max_length=200, 
        verbose_name ='Descripción del proveedor', 
        null=True,
        blank= True,
    )
    address=models.CharField(
        max_length=250,
        null=True, blank=True
        )
    contact=models.CharField(
        max_length=100
    )
    telephone=models.CharField(
        max_length=10,
        null=True, blank=True
    )
    email=models.CharField(
        max_length=250,
        null=True, blank=True
    )

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = "Proveedores"

class Category(BaseManage):
    name = models.CharField(
        max_length=150,
        help_text="Nombre de la categoría",
        null=False,
        blank= False,
    )
    description = models.CharField(
        max_length=150,
        help_text="Descripción de la categoría",
        null=True,
        blank= True,
    )
    def __str__(self):
        return '{}'.format(self.name)
    class Meta:
        verbose_name_plural = "Categorias"

class Product(BaseManage):
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL,
        null=True,
        verbose_name= "Categoría relacionada"
    )
    name = models.CharField(
        max_length=150,
        help_text="Nombre del producto",
        null=False,
        blank= False,
    )
    description = models.CharField(
        max_length=200, 
        verbose_name ='Descripción del Producto', 
        unique=True,
        null=True,
        blank= True,
    )
    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = "Productos"

class Shopping(BaseManage):
    supplier_asociate = models.ForeignKey(
        Provider, on_delete=models.SET_NULL,
        null=True,
        verbose_name= "Proveedor"
    )
    purchase_date=models.DateField(
        verbose_name="Fecha de Compra",
        default=timezone.now,
        blank=True,
        null=True
        )
    number_invoice_provider=models.CharField(
        max_length=100,
        verbose_name="Numero de Factura Proveedor",
        blank=True,
        null=True
        )
    
    sub_total=models.FloatField(        
        verbose_name="Sub Total",
        blank=True,
        null=True,
        default=0
        )
    discount=models.FloatField(        
        verbose_name="Descuento",
        blank=True,
        null=True,
        default=0
        )
    total=models.FloatField(        
        verbose_name="Total",
        blank=True,
        null=True,
        default=0
        )

    deleted_supplier_name = models.CharField(
        max_length=255,
        verbose_name="Nombre del proveedor (eliminado)",
        blank=True,
        null=True
    )
    def __str__(self):
        return '{}'.format(self.number_invoice_provider)

    def save(self, *args, **kwargs):
        if self.supplier_asociate is None:
            # Si la relación del proveedor se elimina, guardar el nombre del proveedor
            self.deleted_supplier_name = self.supplier_asociate.name if self.supplier_asociate else None
            self.supplier_asociate = None  # Establecer la relación del proveedor a None

        if self.sub_total == None  or self.discount == None:
            self.sub_total = 0
            self.discount = 0           
        self.total = self.sub_total - self.discount

        super(Shopping, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Compras"
        verbose_name="Compra"

class ShoppingDetail(BaseManage):
    purchase = models.ForeignKey(
        Shopping, on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name= "Compra"
    )
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name= "Producto"
    )
    amount=models.BigIntegerField(        
        verbose_name="Cantidad",
        blank=False,
        null=False,
        default=0
        )
    price=models.IntegerField(        
        verbose_name="Valor Unitario",
        blank=False,
        null=False,
        default=0
        )
    sub_total=models.IntegerField(        
        verbose_name="Sub Total",
        blank=True,
        null=True,
        default=0
        )
    discount=models.IntegerField(        
        verbose_name="Descuento",
        blank=True,
        null=True,
        default=0
        )
    total=models.IntegerField(        
        verbose_name="Total",
        blank=True,
        null=True,
        default=0
        )
    def __str__(self):
        return '{}'.format(self.product)

    def save(self, *args, **kwargs):
        self.sub_total = float(float(int(self.amount)) * float(self.price))
        self.total = self.sub_total - float(self.discount)
        super(ShoppingDetail, self).save(*args, **kwargs)
    
    class Mega:
        verbose_name_plural = "Detalles Compras"
        verbose_name="Detalle Compra"

class Inventory(BaseManage):
    name = models.CharField(
        max_length=150,
        help_text="Nombre del producto",
        null=False,
        blank= False,
    )
    product = models.ForeignKey(
        Product, on_delete=models.SET_NULL,
        null=True,
        verbose_name= "Producto / Artículo"
    )
    current_existence = models.IntegerField(
        default=0,
        verbose_name = "Existencia en Inventarios",
        null=False,
        blank= False,
    )
    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = "Inventario"
        verbose_name = "Inventarios"

def detalle_compra_borrar(sender,instance, **kwargs):
    id_producto = instance.product.id
    id_purchase = instance.purchase.id

    enc = Shopping.objects.filter(pk=id_purchase).first()
    if enc:
        sub_total = ShoppingDetail.objects.filter(compra=id_purchase).aggregate(Sum('sub_total'))
        descuento = ShoppingDetail.objects.filter(compra=id_purchase).aggregate(Sum('discount'))
        enc.sub_total=sub_total['sub_total__sum']
        enc.discount=descuento['discount__sum']
        enc.save()
    

class OrdenServicio(BaseManage):
    STATE_OPTIONS = [
        ('open', 'Abierta'),
        ('in_progress', 'En progreso'),
        ('close', 'Cerrada'),
    ]
    TYPE_SERVICE = [
        ('maintenance', 'Mantenimiento'),
        ('suspend/reactivate', 'Suspender o Reactivar'),
        ('new_installation', 'Nueva Istalación'),
        ('subscriber_cancellation', 'Baja Suscriptor'),
    ]
    name = models.CharField(
        max_length=150,
        help_text="Orden de servicio",
        null=False,
        blank= False,
    )
    suscriber_asociate = models.ForeignKey(
        Suscriber, on_delete=models.SET_NULL,
        null=True,
        blank= True,
        verbose_name= "Suscriptor Asociado"
    )
    comentary_service = models.TextField(
        verbose_name = "Comentarios o novedades",
        null=True,
        blank= True,
    )
    product_used = models.ManyToManyField(
        Product,
        null=True,
        blank= True,
        verbose_name= "Producto Usado/Instalado"
    )
    type_service = models.CharField(
        verbose_name="Motivo servicio",
        choices=TYPE_SERVICE,
        max_length = 25,
        default='maintenance',
    )
    # count_product = models.PositiveIntegerField(
    #     verbose_name="Cantidad del producto usado",
    #     blank=True,
    #     null=True,
    #     default=0
    # )
    close_date = models.DateTimeField(
        verbose_name="Fecha Cierre Orden",
        default=None,
        blank=True,
        null=True
    )
    status_orders_service = models.CharField(
        verbose_name="Estado de Orden de servicio",
        choices=STATE_OPTIONS,
        max_length = 15,
        default='open',
    )
    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = "Ordenes de servicio"
        verbose_name = "Ordenes de servicio"