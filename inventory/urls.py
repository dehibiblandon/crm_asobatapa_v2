from django.contrib import admin
from django.urls import path
from .views import (
    agregar_detalle_compra,
    obtener_detalles_compra,
    eliminar_item_detalle_compra,
    ProviderListView,
    ProviderDetailView,
    ProviderCreateView,
    ProviderUpdateView,
    ProviderDeleteView,
    CategoryListView, 
    CategoryDetailView, 
    CategoryCreateView, 
    CategoryUpdateView, 
    CategoryDeleteView, 
    ProductListView,
    ProductDetailView,
    ProductCreateView,
    ProductUpdateView,
    ProductDeleteView,
    ShoppingListView,
    ShoppingDetailView,
    FilterProductsByCategoryView,
    ShoppingCreateView,
    ShoppingUpdateView,
    ShoppingDeleteView,
    InventoryListView,
    InventoryDetailView,
    InventoryCreateView,
    InventoryUpdateView,
    InventoryDeleteView,
    OrdenServicioListView,
    OrdenServicioDetailView,
    OrdenServicioCreateView,
    OrdenServicioUpdateView,
    OrdenServicioDeleteView
)
app_name = 'inventory'
urlpatterns = [
    path('proveedores/', ProviderListView.as_view(), name='provider_list'),
    path('proveedores/<int:pk>/', ProviderDetailView.as_view(), name='provider_detail'),
    path('proveedores/new/', ProviderCreateView.as_view(), name='provider_create'),
    path('proveedores/<int:pk>/edit/', ProviderUpdateView.as_view(), name='provider_edit'),
    path('proveedores/<int:pk>/delete/', ProviderDeleteView.as_view(), name='provider_delete'),

    path('categorias/', CategoryListView.as_view(), name='category_list'),
    path('categorias/<int:pk>/', CategoryDetailView.as_view(), name='category_detail'),
    path('categorias/new/', CategoryCreateView.as_view(), name='category_create'),
    path('categorias/<int:pk>/edit/', CategoryUpdateView.as_view(), name='category_edit'),
    path('categorias/<int:pk>/delete/', CategoryDeleteView.as_view(), name='category_delete'),
    #CRUD PRODUCTO

    path('productos/', ProductListView.as_view(), name='product_list'),
    path('productos/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('productos/new/', ProductCreateView.as_view(), name='product_create'),
    path('productos/<int:pk>/edit/', ProductUpdateView.as_view(), name='product_edit'),
    path('productos/<int:pk>/delete/', ProductDeleteView.as_view(), name='product_delete'),

    #CRUD COMPRAS

    path('compras/', ShoppingListView.as_view(), name='shopping_list'),
    path('compras/<int:pk>/', ShoppingDetailView.as_view(), name='shopping_detail'),
    path('compras/new/', ShoppingCreateView.as_view(), name='shopping_create'),
    path('compras/filter-products/', FilterProductsByCategoryView.as_view(), name='filter_products_by_category'),
    path('compras/<int:pk>/edit/', ShoppingUpdateView.as_view(), name='shopping_edit'),
    path('compras/<int:pk>/delete/', ShoppingDeleteView.as_view(), name='shopping_delete'),

    path('compras/<int:compra_id>/agregar_detalle_compra/', agregar_detalle_compra, name='agregar_detalle_compra'),
    path('compras/obtener_detalles_compra/<int:compra_id>/', obtener_detalles_compra, name='obtener_detalles_compra'),
    path('compras/eliminar_detalle_compra/<int:item_compra_id>/', eliminar_item_detalle_compra, name='eliminar_item_detalle_compra'),
    #CRUD PRODUCTO

    path('inventarios/', InventoryListView.as_view(), name='inventory_list'),
    path('inventarios/<int:pk>/', InventoryDetailView.as_view(), name='inventory_detail'),
    path('inventarios/new/', InventoryCreateView.as_view(), name='inventory_create'),
    path('inventarios/<int:pk>/edit/', InventoryUpdateView.as_view(), name='inventory_edit'),
    path('inventarios/<int:pk>/delete/', InventoryDeleteView.as_view(), name='inventory_delete'),

     #CRUD ORDENES DE SERVICIO

    path('orden-servicio/', OrdenServicioListView.as_view(), name='service_list'),
    path('orden-servicio/<int:pk>/', OrdenServicioDetailView.as_view(), name='service_detail'),
    path('orden-servicio/new/', OrdenServicioCreateView.as_view(), name='service_create'),
    path('orden-servicio/<int:pk>/edit/', OrdenServicioUpdateView.as_view(), name='service_edit'),
    path('orden-servicio/<int:pk>/delete/', OrdenServicioDeleteView.as_view(), name='service_delete'),
]