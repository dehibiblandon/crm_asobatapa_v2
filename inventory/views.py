from django.shortcuts import render
from django.db.models import Sum, F
from common.models import UserProfile
from .models import Provider, Shopping, ShoppingDetail, Category, Product, Inventory, OrdenServicio, CashBalance
from django.views import View
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.utils import timezone, dateformat
from django.urls import reverse_lazy
from .form import ProviderForm, ShoppingForm, ShoppingDetailForm, CategoryForm, ProductForm, InventoryForm, OrdenServicioForm

# Create your views here.
#CRUD Provider
class ProviderListView(ListView):
    model = Provider
    template_name = 'provider/provider_list.html'
    context_object_name = 'providers'

class ProviderDetailView(DetailView):
    model = Provider
    template_name = 'suscriber_detail.html'
    context_object_name = 'provider'

class ProviderCreateView(CreateView):
    model = Provider
    template_name = 'provider/provider_form.html'
    context_object_name = "provider"
    form_class = ProviderForm
    success_url = reverse_lazy('inventory:provider_list')
    success_message = "Proveedor Creado"

    # agregamos el usuario que realiza la modificación
    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.created_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        return response

class ProviderUpdateView(UpdateView):
    model = Provider
    template_name = 'provider/provider_form.html'
    success_url = reverse_lazy('inventory:provider_list')
    context_object_name = "provider"
    form_class = ProviderForm
    success_message = "Categoria Actualizada"

    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.modified_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        return response

class ProviderDeleteView(DeleteView):
    model = Provider
    success_url = reverse_lazy('inventory:provider_list')

    def delete(self, request, *args, **kwargs):
        suscriber = self.get_object()
        suscriber.delete()
        return JsonResponse({'success': True, 'message': 'Proveedor EliminadO exitosamente.'})

class CategoryListView(ListView):
    model = Category
    template_name = 'category/category_list.html'
    context_object_name = 'categories'

class CategoryDetailView(DetailView):
    model = Category
    template_name = 'suscriber_detail.html'
    context_object_name = 'category'

class CategoryCreateView(CreateView):
    model = Category
    template_name = 'category/category_form.html'
    context_object_name = "category"
    form_class = CategoryForm
    success_url = reverse_lazy('inventory:category_list')
    success_message = "Categoria Creada"

    # agregamos el usuario que realiza la modificación
    def form_valid(self, form):        
        form.instance.user_modified = self.request.user.username
        form.instance.created_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        return response

class CategoryUpdateView(UpdateView):
    model = Category
    template_name = 'category/category_form.html'
    success_url = reverse_lazy('inventory:category_list')
    context_object_name = "category"
    form_class = CategoryForm
    success_message = "Categoria Actualizada"

    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.modified_date = timezone.now()
                # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        return response

class CategoryDeleteView(DeleteView):
    model = Category
    success_url = reverse_lazy('inventory:category_list')

    def delete(self, request, *args, **kwargs):
        suscriber = self.get_object()
        suscriber.delete()
        return JsonResponse({'success': True, 'message': 'Categoría Eliminada exitosamente.'})
    
#CRUD PRODUCTO

class ProductListView(ListView):
    model = Product
    template_name = 'product/product_list.html'
    context_object_name = 'products'

class ProductDetailView(DetailView):
    model = Product
    template_name = 'product/product_detail.html'
    context_object_name = 'product'

class ProductCreateView(CreateView):
    model = Product
    template_name = 'product/product_form.html'
    context_object_name = "product"
    form_class = ProductForm
    success_url = reverse_lazy('inventory:product_list')
    success_message = "Producto Creado"

    # agregamos el usuario que realiza la modificación
    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.created_date = timezone.now()
                # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        return response

class ProductUpdateView(UpdateView):
    model = Product
    template_name = 'product/product_form.html'
    success_url = reverse_lazy('inventory:product_list')
    context_object_name = "product"
    form_class = ProductForm
    success_message = "Producto Actualizado"

    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.modified_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        return response

class ProductDeleteView(DeleteView):
    model = Product
    success_url = reverse_lazy('inventory:product_list')

    def delete(self, request, *args, **kwargs):
        suscriber = self.get_object()
        suscriber.delete()
        return JsonResponse({'success': True, 'message': 'Producto Eliminado exitosamente.'})

#CRUD COMPRAS

class ShoppingListView(ListView):
    model = Shopping
    template_name = 'shopping/shopping_list.html'
    context_object_name = 'purchases'

class ShoppingDetailView(DetailView):
    model = Shopping
    template_name = 'shopping/shopping_detail.html'
    context_object_name = 'purchase'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['available_products'] = Product.objects.all()
        context['categories'] = Category.objects.all()  # Añade las categorías
        return context
    
class FilterProductsByCategoryView(View):
    def get(self, request, *args, **kwargs):
        category_id = request.GET.get('category_id')
        products = Product.objects.filter(category_id=category_id) if category_id else Product.objects.all()
        product_list = [{'id': product.id, 'name': product.name} for product in products]
        return JsonResponse({'products': product_list})
    
class ShoppingCreateView(CreateView):
    model = Shopping
    template_name = 'shopping/shopping_form.html'
    context_object_name = "purchase"
    form_class = ShoppingForm
    success_message = "Compra Creada"

    # agregamos el usuario que realiza la modificación
    def form_valid(self, form):
        
        form.instance.user_modified = self.request.user.username
        form.instance.created_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
   
        return response
    def get_success_url(self):
        pk = self.object.pk
        return reverse_lazy('inventory:shopping_detail', kwargs={'pk': pk})
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
class ShoppingUpdateView(UpdateView):
    model = Shopping
    template_name = 'shopping/shopping_form.html'
    context_object_name = "purchase"
    form_class = ShoppingForm
    success_message = "Compra Actualizado"

    def form_valid(self, form):
        form.instance.purchase_date = timezone.now()
        form.instance.user_modified = self.request.user.username
        form.instance.modified_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        form.instance.save()
        return response
    
    def get_success_url(self):
        pk = self.object.pk
        return reverse_lazy('inventory:shopping_detail', kwargs={'pk': pk})
    
class ShoppingDeleteView(DeleteView):
    model = Shopping
    success_url = reverse_lazy('inventory:shopping_list')

    def delete(self, request, *args, **kwargs):
        suscriber = self.get_object()
        suscriber.delete()
        return JsonResponse({'success': True, 'message': 'Compra Eliminada exitosamente.'})

def actualizar_inventario(user_company, producto, cantidad):
    try:
        # Obtener el último registro del Modelo de Inventario para el producto proporcionado
        inventario = Inventory.objects.filter(product=producto).latest('id')

        # Sumar la cantidad proporcionada al campo current_existence
        inventario.current_existence = F('current_existence') + cantidad
        inventario.company = user_company
        # Guardar el registro actualizado
        inventario.save()
        
        return True
    except Inventory.DoesNotExist:
        # Si no existe ningún registro de inventario para el producto, crear uno nuevo
        name = f"Inventario {producto.name} {dateformat.format(timezone.localtime(timezone.now()), 'd-m-Y')}"


        Inventory.objects.create(name = name, product=producto, current_existence=cantidad, company=user_company)
        return True
    except Exception as e:
        print(f"Error al actualizar el inventario: {str(e)}")
        return False

    
def agregar_detalle_compra(request, compra_id):
    # ObtenGO la compañía del usuario autenticado
    user_profile = UserProfile.objects.get(user=request.user)
    user_company = user_profile.company


    if request.method == 'POST':
        try:
            # Obtener la compra relacionada con el ID proporcionado
            purchase = Shopping.objects.get(pk=compra_id)
        except Shopping.DoesNotExist:
            return JsonResponse({'success': False, 'message': 'La compra no existe'})
        
        # Obtener los datos del formulario
        category = int(request.POST.get('category_id'))
        product_id = int(request.POST.get('product_id'))
        amount = int(request.POST.get('amount'))
        price = int(request.POST.get('price'))
        sub_total = int(request.POST.get('sub_total'))
        discount = int(request.POST.get('discount'))
        total = int(request.POST.get('total'))
        
        try:
            # Obtener el objeto del producto utilizando su ID
            product = Product.objects.get(pk=product_id)
            # Actualizar el inventario

            if not actualizar_inventario(user_company, product, amount):
                ##si el inventario no existe se debe de crear
                return JsonResponse({'success': False, 'message': 'El inventario no existe'})

        except Product.DoesNotExist:
            return JsonResponse({'success': False, 'message': 'El producto no existe'})
        
        # Guardar el nuevo detalle de compra en la base de datos
        nuevo_detalle = ShoppingDetail.objects.create(
            purchase=purchase,
            product=product,
            amount=amount,
            price=price,
            sub_total=sub_total,
            discount=discount,
            total=total,
            company= user_company,
        )
        #obtener toda la suma del subtotal, descuento y total de todos los items asociados a dicha
        #actualizar el total del encabezado de la compra
        # Obtener todas las ShoppingDetail asociadas al purchase
        shopping_details = ShoppingDetail.objects.filter(purchase=purchase)

        # Calcular la suma de sub_total, discount y total
        # GUARDA LOS DATOS EN LA TABLA DE LA COMPRA EN SI 
        total_sub_total = shopping_details.aggregate(total_sub_total=Sum('sub_total'))['total_sub_total']
        total_discount = shopping_details.aggregate(total_discount=Sum('discount'))['total_discount']
        total_total = shopping_details.aggregate(total_total=Sum('total'))['total_total']

        # Actualizar los campos en el objeto purchase
        purchase.sub_total = total_sub_total
        purchase.discount = total_discount
        purchase.total = total_total
        purchase.save()
        
        
            # Crear una entrada en CashBalance para el Egreso correspondiente a la compra de materiales
        try:
            CashBalance.objects.create(
                amount=total,
                transaction_type='Egreso',
                description=f'Registro de Compra {purchase.id} - {purchase.number_invoice_provider} - {purchase.supplier_asociate}' ,
                company= user_company
            )
        except Exception as e:
            raise e
        # Enviar una respuesta JSON indicando el éxito
        return JsonResponse({'success': True})
    else:
        # Si no es una solicitud AJAX o no es un método POST, enviar una respuesta de error
        return JsonResponse({'success': False, 'message': 'Error en la solicitud'})


def obtener_detalles_compra(request, compra_id):
    # Filtra los detalles de la compra por el ID de la compra
    detalles_compra = ShoppingDetail.objects.filter(purchase_id=compra_id)
    # Calcula la suma de los campos "total"
    total_items_purchase = detalles_compra.aggregate(total=Sum('total'))['total']
    #luego actualizamos el total de la compra
    compra = Shopping.objects.get(pk=compra_id)
    compra.total = total_items_purchase
    compra.save()

    # Serializa los detalles de la compra para convertirlos a JSON
    detalles_json = [{'categoria': detalle.product.category.name, 'producto': detalle.product.name, 'cantidad': detalle.amount, 'precio_unitario': detalle.price, 'sub_total': detalle.sub_total, 'discount': detalle.discount, 'total': detalle.total, 'id':detalle.id} for detalle in detalles_compra]

    # Devuelve los detalles de la compra como una respuesta JSON
    return JsonResponse(detalles_json, safe=False)

def eliminar_item_detalle_compra(request, item_compra_id):
    try:
        # Obtener el detalle de compra
        item_purchase = ShoppingDetail.objects.get(pk=item_compra_id)
        id_purchase = item_purchase.purchase.id
        # Eliminar el detalle de compra
        item_purchase.delete()

        purchase = Shopping.objects.get(pk=id_purchase)
        # Obtener todas las ShoppingDetail asociadas al purchase
        shopping_details = ShoppingDetail.objects.filter(purchase=purchase)

        # Calcular la suma de sub_total, discount y total
        total_sub_total = shopping_details.aggregate(total_sub_total=Sum('sub_total'))['total_sub_total']
        total_discount = shopping_details.aggregate(total_discount=Sum('discount'))['total_discount']
        total_total = shopping_details.aggregate(total_total=Sum('total'))['total_total']

        # Actualizar los campos en el objeto purchase
        purchase.sub_total = total_sub_total
        purchase.discount = total_discount
        purchase.total = total_total
        purchase.save()

        ##Crear logica para que el valor de la compra modificada actualice el registro de balance 
        # Devolver una respuesta de éxito
        return JsonResponse({'success': True})
    except ShoppingDetail.DoesNotExist:
        # Manejar el caso donde el detalle de compra no existe
        return JsonResponse({'success': False, 'message': 'El detalle de compra no existe'}, status=404)
    except Exception as e:
        # Manejar otros errores
        return JsonResponse({'success': False, 'message': str(e)}, status=500)
#CRUD INVENTARIO
    
class InventoryListView(ListView):
    model = Inventory
    template_name = 'inventory/inventory_list.html'
    context_object_name = 'inventories'

class InventoryDetailView(DetailView):
    model = Inventory
    template_name = 'inventory/inventory_detail.html'
    context_object_name = 'inventory'

class InventoryCreateView(CreateView):
    model = Inventory
    template_name = 'inventory/inventory_form.html'
    context_object_name = "inventory"
    form_class = InventoryForm
    success_url = reverse_lazy('inventory:inventory_list')
    success_message = "Inventario Creado"

    # agregamos el usuario que realiza la modificación
    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.created_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        return response

class InventoryUpdateView(UpdateView):
    model = Inventory
    template_name = 'inventory/inventory_form.html'
    success_url = reverse_lazy('inventory:inventory_list')
    context_object_name = "inventory"
    form_class = InventoryForm
    success_message = "Inventario Actualizado"

    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.modified_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        return response

class InventoryDeleteView(DeleteView):
    model = Inventory
    success_url = reverse_lazy('inventory:inventory_list')

    def delete(self, request, *args, **kwargs):
        suscriber = self.get_object()
        suscriber.delete()
        return JsonResponse({'success': True, 'message': 'Inventario Eliminado exitosamente.'})
    

#CRUD ORDEN DE SERVICIO

class OrdenServicioListView(ListView):
    model = OrdenServicio
    template_name = 'service/service_list.html'
    context_object_name = 'services'

class OrdenServicioDetailView(DetailView):
    model = OrdenServicio
    template_name = 'service/service_detail.html'
    context_object_name = 'service'

class OrdenServicioCreateView(CreateView):
    model = OrdenServicio
    template_name = 'service/service_form.html'
    context_object_name = "service"
    form_class = OrdenServicioForm
    success_url = reverse_lazy('inventory:service_list')
    success_message = "Orden Creado"

    # agregamos el usuario que realiza la modificación
    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.created_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        response = super().form_valid(form)
        return response

class OrdenServicioUpdateView(UpdateView):
    model = OrdenServicio
    template_name = 'service/service_form.html'
    success_url = reverse_lazy('inventory:service_list')
    context_object_name = "service"
    form_class = OrdenServicioForm
    success_message = "Inventario Actualizado"

    def form_valid(self, form):
        form.instance.user_modified = self.request.user.username
        form.instance.modified_date = timezone.now()
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #ASIGNAR COMPAÑIA
        form.instance.company = user_company
        #cerrar si el estado es cerrado
        # poner fecha de cerrado solo si en el formulario el campo status_orders_service es close
        #capturamos el valor 
        status_orders_service = form.cleaned_data.get('status_orders_service')
        # Verificar si el estado es 'close' o 'Cerrada'
        if status_orders_service == 'close' or status_orders_service == 'Cerrada':
            form.instance.close_date = timezone.now()
            
        response = super().form_valid(form)
        return response

class OrdenServicioDeleteView(DeleteView):
    model = OrdenServicio
    success_url = reverse_lazy('inventory:service_list')

    def delete(self, request, *args, **kwargs):
        suscriber = self.get_object()
        suscriber.delete()
        return JsonResponse({'success': True, 'message': 'Orden de servicio Eliminado exitosamente.'})