from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from django.core.exceptions import ValidationError
from django.utils import timezone
from PIL import Image

def validate_logo_dimensions(image):
    with Image.open(image) as img:
        width, height = img.size
        if width != 150 or height != 70:
            raise ValidationError('La imagen debe tener exactamente 150x150 px.')
        
# Create your models here.
class BlockPosition(models.Model):
    POSITION_IMAGE =(
        ('left','Izquierda'),
        ('right','Derecha')
    )
    title_block = models.CharField(
        verbose_name="Título del Bloque",
        max_length=60,
        blank=False,
        null=False,
        default=''
    )
    description_block = RichTextField(
        verbose_name="Descripción del Bloque",
        blank=False,
        null=False,
        default=''
    )
    image_aside_block = models.ImageField(
        verbose_name="Imagen Lateral del Bloque",
        upload_to='home_page/block',
        blank=True,
        null=True
    ) 
    position_image = models.CharField(
        verbose_name="Posición imagen",
        choices=POSITION_IMAGE,
        default='right',
        max_length=15
    )
    pre_home = models.ForeignKey(
        'PreHome', on_delete=models.CASCADE, related_name='block_positions',
        blank=True, null=True
    )
    def __str__(self):
        return self.title_block

class BlockFeatures(models.Model):
    title_feature = models.CharField(
        verbose_name="Título de la característica",
        max_length=60,
        blank=False,
        null=False,
        default=''
    )
    description_feature = RichTextField(
        verbose_name="Descripción de la característica",
        blank=False,
        null=False,
        default=''
    )
    logo_feature = models.ImageField(
        verbose_name="Logo Característica",
        upload_to='home_page/feature',
        blank=True,
        null=True,
        validators=[validate_logo_dimensions]
    ) 
    pre_home = models.ForeignKey(
        'PreHome', on_delete=models.CASCADE, related_name='block_feature',
        blank=True, null=True
    )
    def __str__(self):
        return self.title_feature

class BaseHome(models.Model):
    POSITION_IMAGE =(
        ('left','Izuierda'),
        ('right','Derecha')
    )
    parallax_background = models.ImageField(
        verbose_name="Imagen del banner",
        upload_to='home_page/banner',
        blank=True,
        null=True
    )
    paralax_image_cover = models.ImageField(
        verbose_name="Imagen paralax",
        upload_to='home_page/banner',
        blank=True,
        null=True
    )
    position_image = models.CharField(
        verbose_name="Posición imagen",
        choices=POSITION_IMAGE,
        default='right',
        max_length=15
    )    
    title_banner = models.CharField(
        verbose_name="Título del banner",
        max_length=60,
        blank=False,
        null=False,
        default=''
    )
    description_banner = models.CharField(
        verbose_name="Descripción del banner",
        max_length=120,
        blank=False,
        null=False,
        default=''
    )
    title_block_about = models.CharField(
        verbose_name="Título del Bloque Quienes Somos",
        max_length=60,
        blank=False,
        null=False,
        default=''
    )
    descrption_block_about = RichTextField(
        verbose_name="Descripción del Bloque Quienes Somos",
        blank=False,
        null=False,
        default=''
    )
    class Meta:
        abstract = True

class Company(models.Model):
    name_company = models.CharField(
        max_length=60,
        verbose_name="Nombre de la Empresa"
        )
    departament = models.CharField(
        max_length=60,
        verbose_name= "Departamento / Ubicación"
        )
    city = models.CharField(
        max_length=60,
        verbose_name= "City / Ubicación"
        )
    address = models.TextField(
        verbose_name="Dirección",
        blank=True, null=True
        )
    name_contact_company = models.CharField(
        max_length=60,
        verbose_name = "Contacto En la Compañía"
        )
    telephone = models.CharField(
        verbose_name="Teléfono de Contacto",
        max_length=15, 
        blank=True, 
        null=True
        )
    email = models.EmailField(
        verbose_name="Email",
        blank=True, 
        null=True)
    logo_company = models.ImageField(
        verbose_name="Imagen lOGO",
        upload_to='companies/logo',
        blank=True,
        null=True,
    ) 
    created_date = models.DateTimeField(
        verbose_name="Publicación",
        default=timezone.now,
        blank=True,
        null=True
    )
    modified_date = models.DateTimeField(
        verbose_name="Actualización",
        default=timezone.now,
        blank=True,
        null=True
    )
    user_modified =  models.CharField(
        max_length=150,
        verbose_name="Usuario que Crea o Modifica",
        blank=True,
        null=True
    )
    def __str__(self):
        return self.name_company
   
    class Meta:
        verbose_name = "Compañía Cliente del Sistema"
        verbose_name_plural = "Compañías Cliente del Sistema"

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.user.username

class BaseManage(models.Model):
    created_date = models.DateTimeField(
        verbose_name="Publicación",
        default=timezone.now,
        blank=True,
        null=True
    )
    modified_date = models.DateTimeField(
        verbose_name="Actualización",
        default=timezone.now,
        blank=True,
        null=True
    )
    user_modified =  models.CharField(
        max_length=150,
        verbose_name="Usuario que Crea o Modifica",
        blank=True,
        null=True
    )
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, 
        default=None,
        related_name="%(class)s_empresa"
        )
    class Meta:
        abstract = True

class PreHome(BaseHome):
    def __str__(self):
        return self.title_banner
    class Meta:
        verbose_name = "Página de inicio General"
        verbose_name_plural = "Páginas de inicio General"
       
class HomePage(BaseHome):  
    company = models.OneToOneField(
        Company, on_delete=models.CASCADE, 
        related_name='company_related'
        )
    def __str__(self):
        return self.title_banner
    
    class Meta:
        verbose_name = "Pag. Inicio por Compañia"
        verbose_name_plural = "Paginas Inicio por Compañia"


def validate_pdf(file):
    if not file.name.endswith('.pdf'):
        raise ValidationError("Solo archivos Pdf Permitidos.")
     
class Projects(BaseManage):
    home_page = models.ForeignKey(
        HomePage, on_delete=models.CASCADE,
        default=None
        )
    header_image = models.ImageField(
        verbose_name="Imagen del proyecto",
        upload_to='home_page/project',
        blank=True,
        null=True
    )
    cover_image = models.ImageField(
        verbose_name="Imagen cubierta del proyecto",
        upload_to='home_page/project',
        blank=True,
        null=True
    )    
    title_project = models.CharField(
        verbose_name="Título del proyecto",
        max_length=60,
        blank=False,
        null=False,
        default=''
    )
    descrption_project = RichTextField(
        verbose_name="Descripción del Proyecto",
        blank=False,
        null=False,
        default=''
    )
    pdf_file = models.FileField(
        upload_to='media/documents/pdfs/',
        validators=[validate_pdf],
        default='',
        blank=True,
        null=True,
        )
    def __str__(self):
        return self.title_project
    
    class Meta:
        verbose_name = "Proyecto"
        verbose_name_plural = "Proyectos"
    
class Reports(BaseManage):
    home_page = models.ForeignKey(
        HomePage, on_delete=models.CASCADE,
        default=None
        ) 
    title_report = models.CharField(
        verbose_name="Título del Reporte",
        max_length=60,
        blank=False,
        null=False,
        default=''
    )
    description_report = RichTextField(
        verbose_name="Descripción del Reporte",
        blank=False,
        null=False,
        default=''
    )
    pdf_file = models.FileField(
        upload_to='media/reports/pdfs/',
        validators=[validate_pdf],
        default='',
        blank=True,
        null=True,
        )
    def __str__(self):
        return self.title_report
    
    class Meta:
        verbose_name = "Reporte"
        verbose_name_plural = "Reportes"