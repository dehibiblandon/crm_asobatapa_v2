from django.shortcuts import render
from .models import PreHome, HomePage, Projects, BlockPosition, BlockFeatures, Company, Reports
from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
# Create your views here.


class RenderPreHome (TemplateView):
    template_name = 'landing_home/prehome.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pre_home = PreHome.objects.filter().first()
        if pre_home:
            block_positions = BlockPosition.objects.filter(pre_home=pre_home)
            block_features = BlockFeatures.objects.filter(pre_home=pre_home)
            block_customers = Company.objects.filter()
            if block_customers:
                context['block_customers'] = block_customers
            context['block_features'] = block_features    
            context['block_positions'] = block_positions
            context['pre_home'] = pre_home
        return context

class RenderHome(TemplateView):
    template_name = 'landing_home/home_page_landing.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #get company
        empresa_id = kwargs.get('empresa_id')
        empresa = get_object_or_404(Company, pk=empresa_id)
        context['empresa'] = empresa
        #render to base_home
        context['company_logo'] = empresa.logo_company
        context['company_name'] = empresa.name_company

        # get home page
        home_page = get_object_or_404(HomePage, company=empresa)
        context['home_page'] = home_page
        
        #Get Projects
        projects = Projects.objects.filter(home_page=home_page)
        context['projects'] = projects

        reports = Reports.objects.filter(home_page=home_page)
        context['reports'] = reports
 
        return context