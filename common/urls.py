from django.contrib import admin
from django.urls import path
from common.views import (
    RenderPreHome,
    RenderHome
    )
app_name = 'common'
urlpatterns = [
    path('prehome/', RenderPreHome.as_view(), name='prehome'),
    path('empresa/<int:empresa_id>/', RenderHome.as_view(), name='homepage'),

]