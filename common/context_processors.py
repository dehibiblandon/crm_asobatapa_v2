from .models import UserProfile

def company_context(request):
    if request.user.is_authenticated and not request.user.is_superuser:
        user_profile = UserProfile.objects.get(user=request.user)
        company = user_profile.company
        userloged = user_profile.user.first_name +' '+ user_profile.user.last_name
        return {
            'company_name': company.name_company,
            'company_logo': company.logo_company.url if company.logo_company else None,
            'userlogedname': userloged,
            'user_company': company
        }
    return {}
