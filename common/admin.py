from django.contrib import admin
from .models import BlockPosition, BlockFeatures, HomePage, PreHome, Projects, Reports, Company, UserProfile

class BlockPositionInline(admin.StackedInline):
    model = BlockPosition
    extra = 1

class BlockFeaturesInline(admin.StackedInline):
    model = BlockFeatures
    extra = 1

class PreHomeAdmin(admin.ModelAdmin):
    inlines = [BlockPositionInline, BlockFeaturesInline]

class ProjectsInline(admin.StackedInline):
    model = Projects
    extra = 1

class ReportsInline(admin.StackedInline):
    model = Reports
    extra = 1

class HomePageAdmin(admin.ModelAdmin):
    inlines = [ProjectsInline, ReportsInline]


admin.site.register(PreHome, PreHomeAdmin)
admin.site.register(HomePage, HomePageAdmin)
admin.site.register(Company)
admin.site.register(UserProfile)
