# Generated by Django 5.0.7 on 2024-03-08 21:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0009_alter_projects_pdf_file_alter_reports_pdf_file'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='logo_company',
            field=models.ImageField(blank=True, null=True, upload_to='companies/logo', verbose_name='Imagen lOGO'),
        ),
    ]
