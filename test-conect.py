import psycopg2
from psycopg2 import sql

try:
    # Establecer la conexión
    connection = psycopg2.connect(
        dbname="crm-admin360",
        user="dehibi",
        password="Dablandonsa-1",
        host="localhost",
        port="5432"
    )
    # Establecer la codificación de cliente a UTF8
    connection.set_client_encoding('UTF8')

    # Crear un cursor para ejecutar consultas
    cursor = connection.cursor()

    # Ejecutar una consulta simple
    cursor.execute("SELECT version();")

    # Obtener el resultado
    db_version = cursor.fetchone()
    print(f"Conectado a la base de datos PostgreSQL, versión: {db_version}")

    # Cerrar el cursor y la conexión
    cursor.close()
    connection.close()
except Exception as error:
    print(f"Error al conectar a la base de datos: {error}")


