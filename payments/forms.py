from django.forms import ModelForm
from .models import PeriodMounth,Payment, PaymentItem, Invoice
from datetime import datetime
from django import forms
from django.forms import modelformset_factory
from django.forms.widgets import CheckboxSelectMultiple

class PeriodMounthCheckboxSelectMultiple(CheckboxSelectMultiple):
    def option_label(self, option_value):
        return PeriodMounth.objects.get(pk=option_value).name_period

class AdditionalItemCheckboxSelectMultiple(CheckboxSelectMultiple):
    def option_label(self, option_value):
        return PeriodMounth.objects.get(pk=option_value).name_period
       
class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = ['invoice', 'amount_paid']


    def __init__(self, *args, **kwargs):
        suscriber_id = kwargs.pop('suscriber_id')
        super(PaymentForm, self).__init__(*args, **kwargs)

        # Filtrar las facturas pendientes del suscriptor
        pending_invoices = Invoice.objects.filter(
            suscriber=suscriber_id,
            payment_status='pending'
        )

        # Crear las opciones del campo de selección
        invoice_choices = [(invoice.id, str(invoice)) for invoice in pending_invoices]

        # Añadir el campo de selección al formulario
        self.fields['invoice'] = forms.ChoiceField(
            choices=[('', 'Seleccione una factura')] + invoice_choices,
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=True
        )
       

PaymentItemFormSet = modelformset_factory(
    PaymentItem,
    fields=('description', 'amount'),
    extra=4,  # este valor para controlar cuántos formularios extra se muestran

)
