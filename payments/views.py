
from django.views import View
from django.http import HttpResponse, JsonResponse
from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.db.models import Q, Max, FloatField, Subquery, OuterRef
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import SimpleDocTemplate, Paragraph
from xhtml2pdf import pisa
from django.views.generic import ListView, DetailView
from .forms import PaymentForm, PaymentItemFormSet
import datetime
from datetime import timedelta
from django.utils import timezone
from .models import Suscriber, Payment, PaymentItem, Invoice, CashBalance
from common.models import UserProfile
from PyPDF2 import PdfReader, PdfWriter
import locale
from django.utils.translation import activate
from dateutil.relativedelta import relativedelta
from django.conf import settings
import os
import logging

#Primero crear el pago#


class SearchSuscriberView(ListView):
    model = Suscriber
    template_name = 'search_suscriber.html'
    context_object_name = 'suscribers'

    def get_queryset(self):
        search_query = self.request.GET.get('search_query', '').strip()  # Elimina espacios en blanco alrededor
        queryset = self.model.objects.none()  # Creamos un queryset vacío

        if search_query:
            # Dividir la cadena de búsqueda en palabras individuales
            search_words = search_query.split()

            # Inicializar un objeto Q vacío para las condiciones de búsqueda
            search_conditions = Q()

            # Agregar condiciones de búsqueda para cada palabra individual
            for word in search_words:
                search_conditions |= Q(name_suscriber__icontains=word) | \
                                    Q(identification_number__icontains=word) | \
                                    Q(last_name_suscribe__icontains=word)

            # Filtrar el queryset utilizando las condiciones de búsqueda
            queryset = self.model.objects.filter(search_conditions)
            
            # Obtener el valor de la última factura y el estado de pago para cada suscriptor
            last_invoice_info = Invoice.objects.filter(suscriber=OuterRef('pk'), invoice_status= 'invoice_valid').order_by('-issuance_date').values('total_amount', 'payment_status')[:1]
            
            # Anotar el valor de la última factura y el estado de pago para cada suscriptor
            queryset = queryset.annotate(
                last_invoice_amount=Subquery(last_invoice_info.values('total_amount')),
                last_payment_status=Subquery(last_invoice_info.values('payment_status'))
            )
            
        return queryset
    
# class GenerateInvoicePDFsView(View):
#     def get(self, request, *args, **kwargs):

#         # ObtenGO la compañía del usuario autenticado
#         user_profile = UserProfile.objects.get(user=self.request.user)
#         user_company = user_profile.company
#         #Configuración de la libreria pdf
#         # Crear el objeto BytesIO para almacenar el PDF final
#         pdf_buffer = BytesIO()

#         # Configurar la respuesta HTTP con el contenido del PDF
#         response = HttpResponse(content_type='application/pdf')
#         response['Content-Disposition'] = 'attachment; filename=facturas_masivas.pdf'

#         #Obtener las variables globales
        
#         individual_pdfs = []

#         facturation_type = settings.FACTURATION_TYPE
#         billing_type = settings.BILLING_TYPE
#         date_now = timezone.now()
#         period_to_pay = self.calculate_period_to_pay(date_now, facturation_type, billing_type)

#         suscribers = Suscriber.objects.filter(company = user_company)
#         # Iterar sobre los suscriptores y generar PDF individuales
#         individual_pdfs = []
        
#         for suscriber in suscribers:
#             pending_balance= 0
#             invoices_pending_total= 0
#             if suscriber.monthly_value_to_pay > 0:
#                 if suscriber.mode_billing == "frequently" or suscriber.mode_billing == "":
#                     #buscar facturas vencidas para dicho suscriptor
#                     invoices_pending = Invoice.objects.filter(company = user_company, suscriber=suscriber, invoice_status='invoice_valid').exclude(payment_status='paid')

#                     #Obtener la cuota para el periodo de la acción
#                     #calcular los saldos pendientes por cuotas de acción al suscribirse
#                     if suscriber.value_paid_for_action > 0 and suscriber.number_cuotes_for_payment > 0:
#                         #obtenemos el periodo que se dio de alta.
#                         month_name = suscriber.created_date.strftime('%B')
#                         #Consultar si el mes esta en el periodo actual, si esta no se debe calcular
#                         if month_name in period_to_pay:
#                             pending_balance= 0
#                         else:                
#                         # Si no esta, se debe calcular los valores de cada cuota
#                             pending_balance = suscriber.pending_balance // suscriber.number_cuotes_for_payment

#                     if suscriber.number_cuotes_for_payment == 0 or suscriber.number_cuotes_for_payment is None:
#                         pending_balance = suscriber.pending_balance

#                     # Calcular la suma de los periodos pendientes
#                     for expired_period in invoices_pending:
#                         #invoices_pending_total  se renderiza en la factua a nivel informativo
#                         invoices_pending_total = expired_period.total_amount +invoices_pending_total
#                     ##Debemos validar el tipo de facturación para calcular el valor mensual a asignar en la factura
#                     if settings.FACTURATION_TYPE == 'monthly':
#                         service_rate = suscriber.monthly_value_to_pay * 1
#                         facturation_type = "Mensual"
#                     elif settings.FACTURATION_TYPE == 'bimonthly':
#                         service_rate = suscriber.monthly_value_to_pay * 2
#                         facturation_type = "Bimensual"
#                     elif settings.FACTURATION_TYPE == 'quarterly':
#                         service_rate = suscriber.monthly_value_to_pay * 4
#                         facturation_type = "Cuatrimestre"
#                     elif settings.FACTURATION_TYPE == 'annual':
#                         #Si se paga el año se multiplica por 12 y se resta la cantidad resultante desde la varible global de tiempo a descontar
#                         service_rate = suscriber.monthly_value_to_pay * 12 - suscriber.monthly_value_to_pay * settings.ANNUAL_PREPAYMENT_INCENTIVE
#                         facturation_type = "Anual"

#                     total_amount = service_rate  + pending_balance
#                     total_amount_for_pdf = service_rate + invoices_pending_total + pending_balance
#                     expiration_date = date_now + timezone.timedelta(days=settings.EXPIRATION_DAYS)
#                     expiration_date_formatted = expiration_date.strftime('%d %B %Y')

#                     html_string = render_to_string('payments/invoice_template.html', {
#                         'suscriber': suscriber,
#                         'date_now': date_now,
#                         'period_to_pay':period_to_pay,
#                         'expired_periods':invoices_pending,
#                         'invoices_pending_total':invoices_pending_total,
#                         'pending_balance':pending_balance,
#                         'total_amount':total_amount_for_pdf,
#                         'expiration_date': expiration_date_formatted,
#                         'facturation_type':facturation_type,
#                         'service_rate':service_rate,
#                         })
#                     pdf = pisa.CreatePDF(BytesIO(html_string.encode('utf-8')), dest=pdf_buffer)

#                     if pdf.err:
#                         return HttpResponse('Error al generar el PDF', status=500)

#                     pdf_buffer.seek(0)
#                     individual_pdfs.append(BytesIO(pdf_buffer.read()))
#                     #Guardar la factura en la Base de datos
#                     invoice = Invoice(
#                         suscriber=suscriber,
#                         issuance_date= date_now,
#                         period_to_pay = period_to_pay,
#                         monthly_value_to_pay = suscriber.monthly_value_to_pay,
#                         pending_balance = pending_balance,
#                         total_amount = total_amount,
#                         payment_status = "pending",
#                         expiration_date = expiration_date,
#                         company = user_company,
#                     )
#                     invoice.save()         
#                 # elif suscriber.mode_billing == "annual":
#                 #     current_year = timezone.now().year
#                 #     # Consultar si en el año en curso ya tiene una factura emitida
#                 #     existing_invoice_annual = Invoice.objects.filter(suscriber=suscriber, issuance_date__year=current_year)
#                 #     # Verificar si existen facturas anuales para el suscriptor en el año actual
#                 #     if existing_invoice_annual.exists(): 
#                 #     #buscar facturas vencidas para dicho suscriptor
#                 #         invoices_pending = Invoice.objects.filter(suscriber=suscriber, payment_status='expired')
#                 #         #calcular los saldos pendientes por cuotas de acción al suscribirse
#                 #         if suscriber.value_paid_for_action > 0 and suscriber.number_cuotes_for_payment > 0:
#                 #             #obtenemos el periodo que se dio de alta.
#                 #             month_name = suscriber.created_date.strftime('%B')
#                 #             #Consultar si el mes esta en el periodo actual, si esta no se debe calcular
#                 #             if month_name in period_to_pay:
#                 #                 pending_balance= 0
#                 #             else:                
#                 #             # Si no esta, se debe calcular los valores de cada cuota
#                 #                 pending_balance = suscriber.pending_balance // suscriber.number_cuotes_for_payment

#                 #         if suscriber.number_cuotes_for_payment == 0 or suscriber.number_cuotes_for_payment is None:
#                 #             pending_balance = suscriber.pending_balance

#                 #         # Calcular la suma de los periodos pendientes
#                 #         for expired_period in invoices_pending:
#                 #             #invoices_pending_total  se renderiza en la factua a nivel informativo
#                 #             invoices_pending_total = expired_period.total_amount +invoices_pending_total
                        
#                 #         #Si se paga el año se multiplica por 12 y se resta la cantidad resultante desde la varible global de tiempo a descontar
#                 #         service_rate = suscriber.monthly_value_to_pay * 12 - suscriber.monthly_value_to_pay * settings.ANNUAL_PREPAYMENT_INCENTIVE
#                 #         facturation_type = "Anual"

#                 #         total_amount = service_rate  + pending_balance
#                 #         total_amount_for_pdf = service_rate + invoices_pending_total + pending_balance
#                 #         expiration_date = date_now + timezone.timedelta(days=settings.EXPIRATION_DAYS)
#                 #         expiration_date_formatted = expiration_date.strftime('%d %B %Y')

#                 #         html_string = render_to_string('payments/invoice_template.html', {
#                 #             'suscriber': suscriber,
#                 #             'date_now': date_now,
#                 #             'period_to_pay':period_to_pay,
#                 #             'expired_periods':invoices_pending,
#                 #             'invoices_pending_total':invoices_pending_total,
#                 #             'pending_balance':pending_balance,
#                 #             'total_amount':total_amount_for_pdf,
#                 #             'expiration_date': expiration_date_formatted,
#                 #             'facturation_type':facturation_type,
#                 #             'service_rate':service_rate,
#                 #             })
#                 #         pdf = pisa.CreatePDF(BytesIO(html_string.encode('utf-8')), dest=pdf_buffer)

#                 #         if pdf.err:
#                 #             return HttpResponse('Error al generar el PDF', status=500)

#                 #         pdf_buffer.seek(0)
#                 #         individual_pdfs.append(BytesIO(pdf_buffer.read()))
#                 #         #Guardar la factura en la Base de datos
#                 #         invoice = Invoice(
#                 #             suscriber=suscriber,
#                 #             issuance_date= date_now,
#                 #             period_to_pay = period_to_pay,
#                 #             monthly_value_to_pay = suscriber.monthly_value_to_pay,
#                 #             pending_balance = pending_balance,
#                 #             total_amount = total_amount,
#                 #             payment_status = "pending",
#                 #             expiration_date = expiration_date,
#                 #         )
#                 #         invoice.save()         

#         # Fusionar los PDF individuales en uno solo
#         merger = PdfWriter()
#         for individual_pdf in individual_pdfs:
#             merger.append(PdfReader(individual_pdf))

#         # Configurar el contenido del PDF final
#         merger.write(response)
#         return response

#     def calculate_period_to_pay(self, date_now, facturation_type, billing_type):
#         original_locale = locale.getlocale()
#         try:
#             locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')

#             if facturation_type == 'monthly' and billing_type == 'current':
#                 return date_now.strftime('%B')
#             if facturation_type == 'bimonthly' and billing_type == 'current':
#                 # Calcular el periodo para bimensual (por ejemplo, Enero-Feb, Marzo-Abr, etc.)
#                 current_month = date_now.strftime('%B')
#                 previous_month = (date_now - relativedelta(months=1)).strftime('%B')
#                 return f"{previous_month}-{current_month}"
#             elif facturation_type == 'quarterly' and billing_type == 'current':
#                 # Calcular el periodo para trimestral (por ejemplo, Ene-Feb-Mar, Abr-May-Jun, etc.)
#                 current_month = date_now.strftime('%B')
#                 previous_three_months = [(date_now - relativedelta(months=i)).strftime('%B') for i in range(1, 4)][::-1]
#                 return "-".join(previous_three_months + [current_month])
#             elif facturation_type == 'annual' and billing_type == 'current':
#                 # Para facturación anual, el periodo es todo el año en curso
#                 return 'Anual'
#             # Facturación Vencida
#             elif facturation_type == 'bimonthly' and billing_type == 'previous':
#                 # Calcular el periodo bimensual anterior a la fecha actual
#                 return f"{(date_now - relativedelta(months=2)).strftime('%B')}-{(date_now - relativedelta(months=1)).strftime('%B')}"
#             elif facturation_type == 'quarterly' and billing_type == 'previous':
#                 # Calcular el periodo trimestral anterior a la fecha actual
#                  return f"{(date_now - relativedelta(months=3)).strftime('%B')}-{(date_now - relativedelta(months=1)).strftime('%B')}"
#             elif facturation_type == 'monthly' and billing_type == 'previous':
#                 # Calcular el nombre del mes anterior a la fecha actual
#                 return (date_now - relativedelta(months=1)).strftime('%B')
#             elif facturation_type == 'annual' and billing_type == 'previous':
#                 # Para facturación anual, el periodo es todo el año en curso
#                 return (date_now - relativedelta(years=1)).strftime('%Y')
#             else:
#                 # Tipo de facturación no reconocido
#                 return 'Desconocido'
#         finally:
#             locale.setlocale(locale.LC_TIME, original_locale)
# Configuración del logger
logger = logging.getLogger(__name__)

class GenerateInvoicePDFsView(View):
    def get(self, request, *args, **kwargs):
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        
        pdf_buffer = BytesIO()
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=facturas_masivas.pdf'
        
        facturation_type = settings.FACTURATION_TYPE
        billing_type = settings.BILLING_TYPE
        date_now = timezone.now()
        period_to_pay = self.calculate_period_to_pay(date_now, facturation_type, billing_type)
        
        suscribers = list(Suscriber.objects.filter(company=user_company, status='active', mode_billing='frequently', monthly_value_to_pay__gt = 1).order_by('id'))

        individual_pdfs = []
        
        i = 0
        while i < len(suscribers):
            suscriber1 = suscribers[i] if i < len(suscribers) else None
            suscriber2 = None

            # Validamos si existe un segundo suscriptor
            if i + 1 < len(suscribers):
                suscriber2 = suscribers[i + 1]
                i += 2  # Avanzamos dos posiciones si hay un segundo suscriptor
            else:
                i += 1  # Si no, avanzamos una posición

            # Generamos el contexto del primer suscriptor, si existe
            context1 = self.generate_invoice_context(suscriber1, date_now, period_to_pay, user_company) if suscriber1 else None

            # Generamos el contexto del segundo suscriptor solo si existe
            context2 = self.generate_invoice_context(suscriber2, date_now, period_to_pay, user_company) if suscriber2 else None
            
            # Si el primer contexto es None, no generamos el PDF
            if not context1:
                continue

            # Renderizamos el HTML del PDF. Solo agregamos context2 si hay un suscriptor válido
            html_string = render_to_string('payments/invoice_template.html', {
                'context1': context1,
                'context2': context2 if suscriber2 else None  # Solo pasa context2 si hay un suscriptor válido
            })

            pdf = pisa.CreatePDF(BytesIO(html_string.encode('utf-8')), dest=pdf_buffer)

            if pdf.err:
                return HttpResponse('Error al generar el PDF', status=500)

            pdf_buffer.seek(0)
            individual_pdfs.append(BytesIO(pdf_buffer.read()))

            # Guardamos la factura del primer suscriptor
            self.save_invoice_to_db(suscriber1, context1, date_now, period_to_pay, user_company)

            # Solo guardamos la factura del segundo suscriptor si existe
            if suscriber2 and context2:
                self.save_invoice_to_db(suscriber2, context2, date_now, period_to_pay, user_company)

        # Combinamos los PDFs individuales
        if individual_pdfs:
            merger = PdfWriter()
            for individual_pdf in individual_pdfs:
                merger.append(PdfReader(individual_pdf))

            merger.write(response)
            return response
        else:
            return HttpResponse('No se generaron PDFs debido a que los contextos estaban vacíos.', status=400)
        
    def calculate_period_to_pay(self, date_now, facturation_type, billing_type):
        original_locale = locale.getlocale()
        try:
            locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
            
            if facturation_type == 'monthly' and billing_type == 'current':
                return date_now.strftime('%B')
            if facturation_type == 'bimonthly' and billing_type == 'current':
                current_month = date_now.strftime('%B')
                previous_month = (date_now - relativedelta(months=1)).strftime('%B')
                return f"{previous_month}-{current_month}"
            elif facturation_type == 'quarterly' and billing_type == 'current':
                current_month = date_now.strftime('%B')
                previous_three_months = [(date_now - relativedelta(months=i)).strftime('%B') for i in range(1, 4)][::-1]
                return "-".join(previous_three_months + [current_month])
            elif facturation_type == 'annual' and billing_type == 'current':
                return 'Anual'
            elif facturation_type == 'bimonthly' and billing_type == 'previous':
                return f"{(date_now - relativedelta(months=2)).strftime('%B')} {(date_now - relativedelta(months=1)).strftime('%B')}"
            elif facturation_type == 'quarterly' and billing_type == 'previous':
                return f"{(date_now - relativedelta(months=3)).strftime('%B')} {(date_now - relativedelta(months=1)).strftime('%B')}"
            elif facturation_type == 'monthly' and billing_type == 'previous':
                return (date_now - relativedelta(months=1)).strftime('%B')
            elif facturation_type == 'annual' and billing_type == 'previous':
                return (date_now - relativedelta(years=1)).strftime('%Y')
            else:
                return 'Desconocido'
        finally:
            locale.setlocale(locale.LC_TIME, original_locale)

    def generate_invoice_context(self, suscriber, date_now, period_to_pay, user_company):
        if suscriber:
            pending_balance = 0
            invoices_pending_total = 0
            if suscriber.monthly_value_to_pay > 0:
                if suscriber.mode_billing == "frequently" or suscriber.mode_billing == "":
                    invoices_pending = Invoice.objects.filter(company=user_company, suscriber=suscriber, invoice_status='invoice_valid').exclude(payment_status='paid')
                    
                    if suscriber.value_paid_for_action > 0 and suscriber.number_cuotes_for_payment > 0:
                        month_name = suscriber.created_date.strftime('%B')
                        if month_name not in period_to_pay:
                            pending_balance = suscriber.pending_balance // suscriber.number_cuotes_for_payment
                    if suscriber.number_cuotes_for_payment == 0 or suscriber.number_cuotes_for_payment is None:
                        pending_balance = suscriber.pending_balance
                    
                    for expired_period in invoices_pending:
                        invoices_pending_total += expired_period.monthly_value_to_pay
                    
                    if settings.FACTURATION_TYPE == 'monthly':
                        service_rate = suscriber.monthly_value_to_pay * 1
                        facturation_type = "Mensual"
                    elif settings.FACTURATION_TYPE == 'bimonthly':
                        service_rate = suscriber.monthly_value_to_pay * 2
                        facturation_type = "Bimensual"
                    elif settings.FACTURATION_TYPE == 'quarterly':
                        service_rate = suscriber.monthly_value_to_pay * 4
                        facturation_type = "Cuatrimestre"
                    elif settings.FACTURATION_TYPE == 'annual':
                        service_rate = suscriber.monthly_value_to_pay * 12 - suscriber.monthly_value_to_pay * settings.ANNUAL_PREPAYMENT_INCENTIVE
                        facturation_type = "Anual"
                    
                    total_amount = service_rate + pending_balance
                    total_amount_for_pdf = service_rate + invoices_pending_total + pending_balance
                    expiration_date = date_now + timedelta(days=settings.EXPIRATION_DAYS)
                    
                    return {
                        'suscriber': suscriber,
                        'date_now': date_now,
                        'period_to_pay': period_to_pay,
                        'expired_periods': invoices_pending,
                        'invoices_pending_total': invoices_pending_total,
                        'pending_balance': pending_balance,
                        'total_amount': total_amount_for_pdf,
                        'expiration_date': expiration_date,
                        'facturation_type': facturation_type,
                        'service_rate': service_rate,
                    }
        return {}

    def save_invoice_to_db(self, suscriber, context, date_now, period_to_pay, user_company):
        if suscriber and context:
            invoice = Invoice(
                suscriber=suscriber,
                issuance_date=date_now,
                period_to_pay=period_to_pay,
                monthly_value_to_pay=context['service_rate'],
                pending_balance=context['pending_balance'],
                total_amount=context['total_amount'],
                payment_status="pending",
                expiration_date=context['expiration_date'],
                company=user_company,
            )
            invoice.save()

class AddPaymentView(View):
    template_name = 'payments/add_payment.html'

    def get(self, request, suscriber_id, *args, **kwargs):
        suscriber = get_object_or_404(Suscriber, id=suscriber_id)
        
        # Obtener todas las facturas emitidas en estado pendiente
        invoices_pending = Invoice.objects.filter(suscriber=suscriber, payment_status='pending', invoice_status='invoice_valid').order_by('-issuance_date')
        invoices_expired = Invoice.objects.filter(suscriber=suscriber, payment_status='expired', invoice_status='invoice_valid')
        # crear un listado de periodos por pagar
        # expired_periods =[]
        # for invoice in invoices_pending:
        #     expired_periods.append(invoice.period_to_pay)
     
        # # Obtener la última factura pendiente del suscriptor
        # latest_invoice = Invoice.objects.filter(suscriber=suscriber, payment_status='pending').order_by('-issuance_date').first()

        return render(request, self.template_name, {
            'suscriber': suscriber,
            'invoices_pending': invoices_pending,
            'invoices_expired':invoices_expired
            })

#generar pdf#
class GenerateInvoiceForSuscriberView(View):
    def get(self, request, *args, **kwargs):
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        #Configuración de la libreria pdf
        # Crear el objeto BytesIO para almacenar el PDF final
        pdf_buffer = BytesIO()

        # Configurar la respuesta HTTP con el contenido del PDF
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=factura_individual.pdf'
        
        #Obtener las variables globales
        date_now = timezone.now()
        individual_pdfs = []
        pending_balance= 0
        invoices_pending_total= 0
        facturation_type = settings.FACTURATION_TYPE
        billing_type = settings.BILLING_TYPE

        #Consultas de los Modelos
        suscriber_id = self.kwargs.get('id_suscriber')
        suscriber = Suscriber.objects.filter(id=suscriber_id).first()

        if suscriber.mode_billing == "frequently" or suscriber.mode_billing == "":
            if suscriber.monthly_value_to_pay >  0:
                if not suscriber:
                    return HttpResponse('Suscriptor no encontrado', status=404)
                
                invoices_pending = Invoice.objects.filter(suscriber=suscriber, invoice_status='invoice_valid').exclude(payment_status='paid')

                period_to_pay = self.calculate_period_to_pay(date_now, facturation_type, billing_type)
                
                #Obtener la cuota para el periodo de la acción
                if suscriber.value_paid_for_action > 0 and suscriber.number_cuotes_for_payment > 0:
                    #obtenemos el periodo que se dio de alta.
                    month_name = suscriber.created_date.strftime('%B')
                    #Consultar si el mes esta en el periodo actual, si esta no se debe calcular
                    if month_name in period_to_pay:
                        pending_balance= 0
                    else:                
                    # Si no esta, se debe calcular los valores de cada cuota
                        pending_balance = suscriber.pending_balance // suscriber.number_cuotes_for_payment

                if suscriber.number_cuotes_for_payment == 0 or suscriber.number_cuotes_for_payment is None:
                    pending_balance = suscriber.pending_balance

                # Calcular la suma de los periodos
                for expired_period in invoices_pending:
                    #invoices_pending_total  se renderiza en la factua a nivel informativo
                    invoices_pending_total += expired_period.monthly_value_to_pay
                ##Debemos validar el tipo de facturación para calcular el valor mensual a asignar en la factura
                if settings.FACTURATION_TYPE == 'monthly':
                    service_rate = suscriber.monthly_value_to_pay * 1
                    facturation_type = "Mensual"
                elif settings.FACTURATION_TYPE == 'bimonthly':
                    service_rate = suscriber.monthly_value_to_pay * 2
                    facturation_type = "Bimensual"
                elif settings.FACTURATION_TYPE == 'quarterly':
                    service_rate = suscriber.monthly_value_to_pay * 4
                    facturation_type = "Cuatrimestre"
                elif settings.FACTURATION_TYPE == 'annual':
                    #Si se paga el año se multiplica por 12 y se resta la cantidad resultante desde la varible global de tiempo a descontar
                    service_rate = suscriber.monthly_value_to_pay * 12 - suscriber.monthly_value_to_pay * settings.ANNUAL_PREPAYMENT_INCENTIVE
                    facturation_type = "Anual"

                total_amount = service_rate  + pending_balance
                total_amount_for_pdf = service_rate + invoices_pending_total + pending_balance
                expiration_date = date_now + timezone.timedelta(days=settings.EXPIRATION_DAYS)
                expiration_date_formatted = expiration_date.strftime('%d %B %Y')

                html_string = render_to_string('invoice_template.html', {
                    'suscriber': suscriber,
                    'date_now': date_now,
                    'period_to_pay':period_to_pay,
                    'expired_periods':invoices_pending,
                    'invoices_pending_total':invoices_pending_total,
                    'pending_balance':pending_balance,
                    'total_amount':total_amount_for_pdf,
                    'expiration_date': expiration_date_formatted,
                    'facturation_type':facturation_type,
                    'service_rate':service_rate,
                    })
                pdf = pisa.CreatePDF(BytesIO(html_string.encode('utf-8')), dest=pdf_buffer)

                if pdf.err:
                    return HttpResponse('Error al generar el PDF', status=500)

                pdf_buffer.seek(0)
                individual_pdfs.append(BytesIO(pdf_buffer.read()))
                #Guardar la factura en la Base de datos
                invoice = Invoice(
                    suscriber=suscriber,
                    issuance_date= date_now,
                    period_to_pay = period_to_pay,
                    monthly_value_to_pay = suscriber.monthly_value_to_pay,
                    pending_balance = pending_balance,
                    total_amount = total_amount,
                    payment_status = "pending",
                    expiration_date = expiration_date,
                    company = user_company,
                )
                invoice.save()
        elif suscriber.mode_billing == "annual":
            current_year = timezone.now().year
            # Consultar si en el año en curso ya tiene una factura emitida
            #existing_invoice_annual = Invoice.objects.filter(suscriber=suscriber, issuance_date__year=current_year)
            # Verificar si existen facturas anuales para el suscriptor en el año actual
            #buscar facturas vencidas para dicho suscriptor
            period_to_pay = "Anual"
            invoices_pending = Invoice.objects.filter(suscriber=suscriber, invoice_status='invoice_valid').exclude(payment_status='paid')

            #calcular los saldos pendientes por cuotas de acción al suscribirse
            if suscriber.value_paid_for_action > 0 and suscriber.number_cuotes_for_payment > 0:
                #obtenemos el periodo que se dio de alta.
                month_name = suscriber.created_date.strftime('%B')
                #Consultar si el mes esta en el periodo actual no se cobra saldos pendientes
                if month_name in period_to_pay:
                    pending_balance= 0
                else:                
                # Si no esta en el periodo actual, se debe calcular los valores de cada cuota
                    pending_balance = suscriber.pending_balance // suscriber.number_cuotes_for_payment

            if suscriber.number_cuotes_for_payment == 0 or suscriber.number_cuotes_for_payment is None:
                pending_balance = suscriber.pending_balance

            # Calcular la suma de los periodos pendientes
            for expired_period in invoices_pending:
                #invoices_pending_total  se renderiza en la factua a nivel informativo
                invoices_pending_total = expired_period.total_amount +invoices_pending_total
            
            #Si se paga el año se multiplica por 12 y se resta la cantidad resultante desde la varible global de tiempo a descontar
            service_rate = suscriber.monthly_value_to_pay * 12 - suscriber.monthly_value_to_pay * settings.ANNUAL_PREPAYMENT_INCENTIVE
            facturation_type = "Anual"

            total_amount = service_rate  + pending_balance
            total_amount_for_pdf = service_rate + invoices_pending_total + pending_balance
            expiration_date = date_now + timezone.timedelta(days=settings.EXPIRATION_DAYS)
            expiration_date_formatted = expiration_date.strftime('%d %B %Y')

            html_string = render_to_string('invoice_template.html', {
                'suscriber': suscriber,
                'date_now': date_now,
                'period_to_pay':period_to_pay,
                'expired_periods':invoices_pending,
                'invoices_pending_total':invoices_pending_total,
                'pending_balance':pending_balance,
                'total_amount':total_amount_for_pdf,
                'expiration_date': expiration_date_formatted,
                'facturation_type':facturation_type,
                'service_rate':service_rate,
                })
            pdf = pisa.CreatePDF(BytesIO(html_string.encode('utf-8')), dest=pdf_buffer)

            if pdf.err:
                return HttpResponse('Error al generar el PDF', status=500)

            pdf_buffer.seek(0)
            individual_pdfs.append(BytesIO(pdf_buffer.read()))
            #Guardar la factura en la Base de datos
            invoice = Invoice(
                suscriber=suscriber,
                issuance_date= date_now,
                period_to_pay = period_to_pay,
                monthly_value_to_pay = suscriber.monthly_value_to_pay,
                pending_balance = pending_balance,
                total_amount = total_amount,
                payment_status = "pending",
                expiration_date = expiration_date,
                company = user_company,
            )
            invoice.save()         

        # Fusionar los PDF individuales en uno solo
        merger = PdfWriter()
        for individual_pdf in individual_pdfs:
            merger.append(PdfReader(individual_pdf))

        # Configurar el contenido del PDF final
        merger.write(response)
        return response

    def calculate_period_to_pay(self, date_now, facturation_type, billing_type):
        original_locale = locale.getlocale()
        try:
            locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')

            if facturation_type == 'monthly' and billing_type == 'current':
                return date_now.strftime('%B')
            if facturation_type == 'bimonthly' and billing_type == 'current':
                # Calcular el periodo para bimensual (por ejemplo, Enero-Feb, Marzo-Abr, etc.)
                current_month = date_now.strftime('%B')
                previous_month = (date_now - relativedelta(months=1)).strftime('%B')
                return f"{previous_month}-{current_month}"
            elif facturation_type == 'quarterly' and billing_type == 'current':
                # Calcular el periodo para trimestral (por ejemplo, Ene-Feb-Mar, Abr-May-Jun, etc.)
                current_month = date_now.strftime('%B')
                previous_three_months = [(date_now - relativedelta(months=i)).strftime('%B') for i in range(1, 4)][::-1]
                return "-".join(previous_three_months + [current_month])
            elif facturation_type == 'annual' and billing_type == 'current':
                # Para facturación anual, el periodo es todo el año en curso
                return 'Anual'
            # Facturación Vencida
            elif facturation_type == 'bimonthly' and billing_type == 'previous':
                # Calcular el periodo bimensual anterior a la fecha actual
                return f"{(date_now - relativedelta(months=2)).strftime('%B')} {(date_now - relativedelta(months=1)).strftime('%B')}"
            elif facturation_type == 'quarterly' and billing_type == 'previous':
                # Calcular el periodo trimestral anterior a la fecha actual
                 return f"{(date_now - relativedelta(months=3)).strftime('%B')} {(date_now - relativedelta(months=1)).strftime('%B')}"
            elif facturation_type == 'monthly' and billing_type == 'previous':
                # Calcular el nombre del mes anterior a la fecha actual
                return (date_now - relativedelta(months=1)).strftime('%B')
            elif facturation_type == 'annual' and billing_type == 'previous':
                # Para facturación anual, el periodo es todo el año en curso
                return (date_now - relativedelta(years=1)).strftime('%Y')
            else:
                # Tipo de facturación no reconocido
                return 'Desconocido'
        finally:
            locale.setlocale(locale.LC_TIME, original_locale)

class RenderInvoiceView(View):
    #@method_decorator(csrf_exempt)  # Decorador para deshabilitar la protección CSRF
    def get(self, request, *args, **kwargs):
        suscriber_id = self.kwargs.get('id_suscriber')
        suscriber = Suscriber.objects.filter(id=suscriber_id).first()
        if not suscriber:
            return HttpResponse('Suscriptor no encontrado', status=404)

        # Obtener la fecha actual
        date_now = datetime.datetime.now()

        # Ruta de la imagen del separador
        path_img_separador = '/static/img/separator-invoice.png'

        # Calcular el periodo de pago
        facturation_type = 'bimonthly'
        billing_type = 'previous'

        # Renderizar el contenido HTML
        html_content = render_to_string('invoice_template.html', {
            'suscriber': suscriber,
            'date_now': date_now,

            'path_img_separador': path_img_separador,
        })

        # Devolver la respuesta HTTP con el contenido HTML renderizado
        return HttpResponse(html_content)
    
class InvoiceList(ListView):
    model = Invoice
    template_name = 'invoice/invoice_list.html'
    context_object_name = 'invoices'

    def get_context_data(self, **kwargs):
        # Obtener el ID del suscriptor desde la URL
        id_suscriber = self.kwargs.get('id_suscriber')
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_company = user_profile.company
        suscriber = Suscriber.objects.filter(company = user_company, pk=id_suscriber)
        
        # Obtener el contexto existente
        context = super().get_context_data(**kwargs)
        
        # Agregar el objeto suscriptor al contexto
        context['suscriber'] = suscriber
        
        return context

    def get_queryset(self):
        # Obtener el ID del suscriptor desde la URL
        id_suscriber = self.kwargs.get('id_suscriber')
        suscriber = get_object_or_404(Suscriber, pk=id_suscriber)
        
        # Filtrar las facturas por el suscriptor
        queryset = Invoice.objects.filter(suscriber=suscriber)
        return queryset

class InvoiceDetailView(DetailView):
    model = Invoice
    template_name = 'invoice/invoice_detail.html'
    context_object_name = 'invoice'
    def get_object(self, queryset=None):
        #get id from url
        id_invoice = self.kwargs.get('id_invoice')

        return self.model.objects.get(pk=id_invoice)

class InvoiceCreateView(DetailView):
    model = Suscriber
    template_name = 'invoice/invoice_create.html'
    context_object_name = 'suscriber'
    # pasar a la vista objeto suscriptor

    
    # similar a la vista de crear compras
    # agregar formulario con los campos de la factura
    # agregar botón de guardar y generar pdf
    
def pay_invoice(request, invoice_id):

    if request.method == 'POST':
        try:
            # ObtenGO la compañía del usuario autenticado
            
            user_profile = UserProfile.objects.get(user=34)
            user_company = user_profile.company
            invoice = Invoice.objects.get(company=user_company, pk=invoice_id)
            invoice.payment_status = 'paid'
            invoice.last_payment_date = timezone.now()
            invoice.save()
            amount = int(invoice.total_amount)
            # Crear una entrada en CashBalance para el ingreso correspondiente al pago de la factura
            try:
                CashBalance.objects.create(
                    amount=amount,
                    transaction_type='Ingreso',
                    description=f'Pago de factura {invoice_id} - {invoice.suscriber.name_suscriber} {invoice.suscriber.last_name_suscribe}' ,
                    company = user_company,
                )
            except Exception as e:
                raise e
            # Devolver un mensaje de éxito en formato JSON
            return JsonResponse({'success': True, 'message': 'Pago registrado exitosamente.'})
        except Invoice.DoesNotExist:
            # Si la factura no existe, devolver un mensaje de error
            return JsonResponse({'success': False, 'message': 'La factura no existe'})

def cancelled_invoice(request, invoice_id):
    if request.method == 'POST':
        try:
            invoice = Invoice.objects.get(pk=invoice_id)
            invoice.invoice_status = 'invoice_cancelled'
            invoice.save()
            return JsonResponse({'success': True, 'message': 'Factura Anulada.'})
        except Invoice.DoesNotExist:
            # Si la factura no existe, devolver un mensaje de error
            return JsonResponse({'success': False, 'message': 'La factura no existe'})
        
def create_invoice_extraordinary(request):  
    if request.method == 'POST':
        
        # ObtenGO la compañía del usuario autenticado
        user_profile = UserProfile.objects.get(user=request.user)
        user_company = user_profile.company
        # Obtener los datos del formulario invoice extra
        id_suscriber = int(request.POST.get('suscriber_id'))
        period = request.POST.get('period')
        description = request.POST.get('description')
        others_values = int(request.POST.get('others_values'))
        others_total = int(request.POST.get('others_total'))
        try:
            # Obtener el suscriptor relacionada con el ID proporcionado
            suscriber = Suscriber.objects.get(pk=id_suscriber)
        except Suscriber.DoesNotExist:
            return JsonResponse({'success': False, 'message': 'El suscriptor no existe'})
        
        #Obtener las variables globales
        date_now = timezone.now()

        if suscriber.mode_billing == "frequently":
            facturation_type = "Periodica"
        elif suscriber.mode_billing == "annual":
            facturation_type = "Anual"

        
        expiration_date = date_now + timezone.timedelta(days=settings.EXPIRATION_DAYS)
        #Guardar la factura en la Base de datos
        invoice = Invoice(
            suscriber=suscriber,
            description = description,
            issuance_date= date_now,
            period_to_pay = facturation_type,
            monthly_value_to_pay = suscriber.monthly_value_to_pay,
            pending_balance = others_values,
            total_amount = others_total,
            payment_status = "pending",
            expiration_date = expiration_date,
            company = user_company            
        )
        invoice.save()
        return JsonResponse({'success': True})
    else:
        # Si no es una solicitud AJAX o no es un método POST, enviar una respuesta de error
        return JsonResponse({'success': False, 'message': 'Error en la solicitud'})
    
def print_invoice(request, invoice_id):
    #Configuración de la libreria pdf
    # Crear el objeto BytesIO para almacenar el PDF final
    pdf_buffer = BytesIO()

    # Configurar la respuesta HTTP con el contenido del PDF
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=factura_individual.pdf'
    
    #Consultas de los Modelos
    invoice = Invoice.objects.filter(pk=invoice_id).first()

    individual_pdfs = []
    html_string = render_to_string('invoice_template.html', {
        'suscriber': invoice.suscriber,
        'description': invoice.description,
        'date_now': invoice.issuance_date,
        'period_to_pay': invoice.period_to_pay,
        'pending_balance': invoice.pending_balance,
        'total_amount': invoice.total_amount,
        'expiration_date': invoice.expiration_date,
        'facturation_type': invoice.suscriber.mode_billing,
        })
    pdf = pisa.CreatePDF(BytesIO(html_string.encode('utf-8')), dest=pdf_buffer)

    if pdf.err:
        return HttpResponse('Error al generar el PDF', status=500)

    pdf_buffer.seek(0)
    individual_pdfs.append(BytesIO(pdf_buffer.read()))
            
    # Fusionar los PDF individuales en uno solo
    merger = PdfWriter()
    for individual_pdf in individual_pdfs:
        merger.append(PdfReader(individual_pdf))

    # Configurar el contenido del PDF final
    merger.write(response)
    return response
