from django.contrib import admin
from .models import PeriodMounth ,Payment, Invoice, CashBalance

# Register your models here.
class InvoiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_suscriber_name', 'issuance_date', 'period_to_pay', 'total_amount', 'last_payment_date', 'payment_status', 'invoice_status')
    list_filter = ('payment_status', 'expiration_date')
    search_fields = ['suscriber__name_suscriber', 'suscriber__last_name_suscribe']  # Buscar por nombre y apellido del suscriptor
    ordering = ('issuance_date',)  # Ordenar por fecha de emisión
    def get_suscriber_name(self, obj):
        return f"{obj.suscriber.name_suscriber} - {obj.suscriber.last_name_suscribe}"
    get_suscriber_name.short_description = 'Suscriptor'  # Etiqueta que aparecerá en la columna

admin.site.register(CashBalance)
admin.site.register(PeriodMounth)
admin.site.register(Payment)
admin.site.register(Invoice, InvoiceAdmin)