# payment_utils.py

from .models import Payment, PaymentItem

def make_payment(suscriber, total_amount, payment_items):
    payment = Payment.objects.create(suscriber=suscriber, total_amount=total_amount)
    
    for item in payment_items:
        PaymentItem.objects.create(payment=payment, description=item['description'], amount=item['amount'])
    
    suscriber.update_pending_balance()
