from django.urls import path
from payments.views import (
    GenerateInvoiceForSuscriberView, 
    SearchSuscriberView, 
    pay_invoice,
    cancelled_invoice,
    create_invoice_extraordinary,
    print_invoice,
    GenerateInvoicePDFsView, 
    AddPaymentView, 
    RenderInvoiceView, 
    InvoiceList,
    InvoiceDetailView,
    InvoiceCreateView
)

app_name = 'payments'
urlpatterns = [
    # ... Otras URLs ...
    path('generate_invoice_pdf/<int:id_suscriber>/', GenerateInvoiceForSuscriberView.as_view(), name='generate_invoice_pdf'),
    path('search/', SearchSuscriberView.as_view(), name='search_suscriber'),
    path('generate-pdf/list/', GenerateInvoicePDFsView.as_view(), name='invoice_list'),
    path('add-payment/<int:suscriber_id>/', AddPaymentView.as_view(), name='add_payment'),
    path('test/<int:id_suscriber>/', RenderInvoiceView.as_view(), name='test'),
    path('historico-factura/<int:id_suscriber>/', InvoiceList.as_view(), name='view_invoice_list'),
    path('historico-factura/detalle/<int:id_invoice>/', InvoiceDetailView.as_view(), name='view_invoice_detail'),
    path('pagar-factura/<int:invoice_id>/', pay_invoice, name='pay_invoice'),
    path('anular-factura/<int:invoice_id>/', cancelled_invoice, name='cancelled_invoice'),
    path('crear-recibo-extemporaneo/', create_invoice_extraordinary, name='create_invoice_extra'),
    path('imprimir-recibo-extemporaneo/<int:invoice_id>', print_invoice, name='print_invoice_extra'),
    path('crear-factura/<int:pk>/', InvoiceCreateView.as_view(), name='create_invoice_pdf'),

    ]
