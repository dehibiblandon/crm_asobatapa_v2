from django.db.models.signals import pre_save
from django.db.models import Sum
from django.dispatch import receiver
from django.db import models
from users.models import  Suscriber, BaseManage
from common.models import UserProfile
from datetime import datetime
from django.utils import timezone
from django.utils.functional import SimpleLazyObject


# Create your models here.
class PeriodMounth(BaseManage):
    FACTURATION_TYPE_CHOICES = [
        ('monthly', 'Mensual'),
        ('bimonthly', 'Bimensual'),
        ('quarterly', 'Trimestral'),
        ('annual', 'Anual'),
    ]

    name_period = models.CharField(
        verbose_name="Nombre del Periodo O Mes",
        max_length=55,
        blank=False,
        null=False,
        default='Enero-Febrero'
    )

    facturation_type = models.CharField(
        max_length=20,
        choices=FACTURATION_TYPE_CHOICES,
        default='monthly',
        verbose_name="Tipo de Facturación"
    )

    def __str__(self):
        return f" {self.name_period}"
    
class Invoice(BaseManage):
    suscriber = models.ForeignKey(
        Suscriber, 
        on_delete=models.CASCADE, 
        related_name='invoices'
        )
    description = models.TextField(
        max_length=150,
        blank=True,
        null=True
    )    
    issuance_date = models.DateField(
        verbose_name="Fecha de Emisión",
        default=timezone.now,
        blank=True,
        null=True
        )
    # cambiar si se necesita
    period_to_pay = models.CharField(
        verbose_name="Nombre del Periodo O Mes",
        max_length=55,
        blank=False,
        null=False,
        default=''
    )
    
    monthly_value_to_pay = models.IntegerField(
        verbose_name="Tarifa Mensual Servicio ",
        blank=False,
        null=False,
        default=0
    )
    pending_balance = models.DecimalField(
        verbose_name="Saldo Pendiente por el Costo de Acción",
        max_digits=10,
        decimal_places=2,
        default=0,
        blank=True,
        null=True
    )    
    total_amount = models.DecimalField(
        max_digits=10, 
        decimal_places=2, 
        verbose_name="Monto Total",
        blank=True,
        null=True)
    
    last_payment_date = models.DateTimeField(
        verbose_name="Fecha de último Pago",
        blank=True,
        null=True
    )
    payment_status = models.CharField(
        max_length=20,
        choices=[('pending', 'Pendiente'), ('paid', 'Pagado'), ('expired', 'Vencido')],
        default='pending',
        verbose_name="Estado de Pago"
    ) 
    expiration_date = models.DateTimeField(
        verbose_name="Fecha de Vencimiento",
        blank=True,
        null=True
    )
    invoice_status = models.CharField(
        max_length=20,
        choices=[('invoice_cancelled', 'Anulada'), ('invoice_valid', 'Valida')],
        default='invoice_valid',
        verbose_name="Estado de Factura"
    )
    

    def __str__(self):
        return f"Factura {self.id} - {self.suscriber.name_suscriber} - {self.suscriber.last_name_suscribe}"
    
@receiver(pre_save, sender=Invoice)
def update_payment_status(sender, instance, **kwargs):
    if instance.expiration_date and instance.expiration_date < timezone.now():
        instance.payment_status = 'paid'
##Payment
class Payment(BaseManage):
    invoice = models.ForeignKey(
        Invoice,
        on_delete=models.CASCADE,
        related_name='payments',
        verbose_name='Factura Pagada'
    )
    payment_date = models.DateTimeField(
        verbose_name="Fecha de Pago",
        default=timezone.now
    )
    amount_paid = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name="Monto Pagado"
    )

    def __str__(self):
        return f"Pago de Factura {self.invoice.id} - {self.amount_paid}"
    
    class Meta:
        verbose_name = "Pago"
        verbose_name_plural = "Pagos"

class PaymentItem(BaseManage):
    payment = models.ForeignKey(
        Payment,
        verbose_name="Pago",
        on_delete=models.CASCADE
    )
    description = models.CharField(
        verbose_name="Descripción",
        max_length=255
    )
    amount = models.DecimalField(
        verbose_name="Monto",
        max_digits=10,
        decimal_places=2,
        default=0
    )
    
    def __str__(self):
        return self.description

class CashBalance(BaseManage):
    TRANSACTION_TYPES = [
        ('Ingreso', 'Ingreso'),
        ('Egreso', 'Egreso')
    ]
    amount = models.IntegerField(
        verbose_name="Saldo En Caja",
        blank=False,
        null=False,
        default=0
    )
    transaction_type = models.CharField(
        max_length=10, 
        choices=TRANSACTION_TYPES
    )
    description = models.CharField(
        max_length=255
    )

    def __str__(self):
        return f"Movimiento {self.transaction_type} - {self.amount}"

    @classmethod
    def get_current_balance(cls, request):
        user_company = SimpleLazyObject(lambda: UserProfile.objects.get(user=request.user).company)
        total_ingresos = cls.objects.filter(company=user_company, transaction_type='Ingreso').aggregate(total=Sum('amount'))['total'] or 0
        total_egresos = cls.objects.filter(company=user_company, transaction_type='Egreso').aggregate(total=Sum('amount'))['total'] or 0
        return total_ingresos - total_egresos